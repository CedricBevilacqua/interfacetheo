﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FenetreParametrage
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.BouttonPlusUnJoueur = New System.Windows.Forms.Button()
        Me.bouttonmoins = New System.Windows.Forms.Button()
        Me.BouttonDemarrer = New System.Windows.Forms.Button()
        Me.BouttonAnnuler = New System.Windows.Forms.Button()
        Me.Labeljoueur1 = New System.Windows.Forms.Label()
        Me.Labeljoueur2 = New System.Windows.Forms.Label()
        Me.TextBoxNomjoueur1 = New System.Windows.Forms.TextBox()
        Me.TextBoxAgejoueur1 = New System.Windows.Forms.TextBox()
        Me.TextBoxNomjoueur2 = New System.Windows.Forms.TextBox()
        Me.TextBoxAgejoueur2 = New System.Windows.Forms.TextBox()
        Me.PanelJoueur3 = New System.Windows.Forms.Panel()
        Me.TextBoxAgejoueur3 = New System.Windows.Forms.TextBox()
        Me.TextBoxNomjoueur3 = New System.Windows.Forms.TextBox()
        Me.LabelJoueur3 = New System.Windows.Forms.Label()
        Me.PanelJoueur4 = New System.Windows.Forms.Panel()
        Me.TextBoxAgejoueur4 = New System.Windows.Forms.TextBox()
        Me.TextBoxNomjoueur4 = New System.Windows.Forms.TextBox()
        Me.LabelJoueur4 = New System.Windows.Forms.Label()
        Me.PanelBoutonsAjouts = New System.Windows.Forms.Panel()
        Me.PanelJoueur3.SuspendLayout()
        Me.PanelJoueur4.SuspendLayout()
        Me.PanelBoutonsAjouts.SuspendLayout()
        Me.SuspendLayout()
        '
        'BouttonPlusUnJoueur
        '
        Me.BouttonPlusUnJoueur.Location = New System.Drawing.Point(48, 3)
        Me.BouttonPlusUnJoueur.Name = "BouttonPlusUnJoueur"
        Me.BouttonPlusUnJoueur.Size = New System.Drawing.Size(30, 23)
        Me.BouttonPlusUnJoueur.TabIndex = 0
        Me.BouttonPlusUnJoueur.Text = "+"
        Me.BouttonPlusUnJoueur.UseVisualStyleBackColor = True
        '
        'bouttonmoins
        '
        Me.bouttonmoins.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bouttonmoins.Location = New System.Drawing.Point(0, 3)
        Me.bouttonmoins.Name = "bouttonmoins"
        Me.bouttonmoins.Size = New System.Drawing.Size(30, 23)
        Me.bouttonmoins.TabIndex = 1
        Me.bouttonmoins.Text = "-"
        Me.bouttonmoins.UseVisualStyleBackColor = True
        '
        'BouttonDemarrer
        '
        Me.BouttonDemarrer.Location = New System.Drawing.Point(12, 447)
        Me.BouttonDemarrer.Name = "BouttonDemarrer"
        Me.BouttonDemarrer.Size = New System.Drawing.Size(75, 23)
        Me.BouttonDemarrer.TabIndex = 2
        Me.BouttonDemarrer.Text = "Démarrer"
        Me.BouttonDemarrer.UseVisualStyleBackColor = True
        '
        'BouttonAnnuler
        '
        Me.BouttonAnnuler.Location = New System.Drawing.Point(94, 447)
        Me.BouttonAnnuler.Name = "BouttonAnnuler"
        Me.BouttonAnnuler.Size = New System.Drawing.Size(75, 23)
        Me.BouttonAnnuler.TabIndex = 3
        Me.BouttonAnnuler.Text = "Annuler"
        Me.BouttonAnnuler.UseVisualStyleBackColor = True
        '
        'Labeljoueur1
        '
        Me.Labeljoueur1.AutoSize = True
        Me.Labeljoueur1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Labeljoueur1.Location = New System.Drawing.Point(91, 20)
        Me.Labeljoueur1.Name = "Labeljoueur1"
        Me.Labeljoueur1.Size = New System.Drawing.Size(74, 18)
        Me.Labeljoueur1.TabIndex = 4
        Me.Labeljoueur1.Text = "Joueur 1"
        '
        'Labeljoueur2
        '
        Me.Labeljoueur2.AutoSize = True
        Me.Labeljoueur2.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Labeljoueur2.Location = New System.Drawing.Point(91, 104)
        Me.Labeljoueur2.Name = "Labeljoueur2"
        Me.Labeljoueur2.Size = New System.Drawing.Size(74, 18)
        Me.Labeljoueur2.TabIndex = 5
        Me.Labeljoueur2.Text = "Joueur 2"
        '
        'TextBoxNomjoueur1
        '
        Me.TextBoxNomjoueur1.Location = New System.Drawing.Point(94, 41)
        Me.TextBoxNomjoueur1.Name = "TextBoxNomjoueur1"
        Me.TextBoxNomjoueur1.Size = New System.Drawing.Size(197, 20)
        Me.TextBoxNomjoueur1.TabIndex = 6
        Me.TextBoxNomjoueur1.Text = "Nom du joueur"
        '
        'TextBoxAgejoueur1
        '
        Me.TextBoxAgejoueur1.Location = New System.Drawing.Point(94, 67)
        Me.TextBoxAgejoueur1.Name = "TextBoxAgejoueur1"
        Me.TextBoxAgejoueur1.Size = New System.Drawing.Size(197, 20)
        Me.TextBoxAgejoueur1.TabIndex = 7
        Me.TextBoxAgejoueur1.Text = "Age du joueur"
        '
        'TextBoxNomjoueur2
        '
        Me.TextBoxNomjoueur2.Location = New System.Drawing.Point(94, 125)
        Me.TextBoxNomjoueur2.Name = "TextBoxNomjoueur2"
        Me.TextBoxNomjoueur2.Size = New System.Drawing.Size(197, 20)
        Me.TextBoxNomjoueur2.TabIndex = 8
        Me.TextBoxNomjoueur2.Text = "Nom du joueur"
        '
        'TextBoxAgejoueur2
        '
        Me.TextBoxAgejoueur2.Location = New System.Drawing.Point(94, 151)
        Me.TextBoxAgejoueur2.Name = "TextBoxAgejoueur2"
        Me.TextBoxAgejoueur2.Size = New System.Drawing.Size(197, 20)
        Me.TextBoxAgejoueur2.TabIndex = 9
        Me.TextBoxAgejoueur2.Text = "Age du joueur "
        '
        'PanelJoueur3
        '
        Me.PanelJoueur3.Controls.Add(Me.TextBoxAgejoueur3)
        Me.PanelJoueur3.Controls.Add(Me.TextBoxNomjoueur3)
        Me.PanelJoueur3.Controls.Add(Me.LabelJoueur3)
        Me.PanelJoueur3.Location = New System.Drawing.Point(12, 178)
        Me.PanelJoueur3.Name = "PanelJoueur3"
        Me.PanelJoueur3.Size = New System.Drawing.Size(810, 103)
        Me.PanelJoueur3.TabIndex = 10
        '
        'TextBoxAgejoueur3
        '
        Me.TextBoxAgejoueur3.Location = New System.Drawing.Point(82, 59)
        Me.TextBoxAgejoueur3.Name = "TextBoxAgejoueur3"
        Me.TextBoxAgejoueur3.Size = New System.Drawing.Size(197, 20)
        Me.TextBoxAgejoueur3.TabIndex = 2
        Me.TextBoxAgejoueur3.Text = "Age du joueur"
        '
        'TextBoxNomjoueur3
        '
        Me.TextBoxNomjoueur3.Location = New System.Drawing.Point(82, 33)
        Me.TextBoxNomjoueur3.Name = "TextBoxNomjoueur3"
        Me.TextBoxNomjoueur3.Size = New System.Drawing.Size(197, 20)
        Me.TextBoxNomjoueur3.TabIndex = 1
        Me.TextBoxNomjoueur3.Text = "Nom du joueur"
        '
        'LabelJoueur3
        '
        Me.LabelJoueur3.AutoSize = True
        Me.LabelJoueur3.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelJoueur3.Location = New System.Drawing.Point(79, 12)
        Me.LabelJoueur3.Name = "LabelJoueur3"
        Me.LabelJoueur3.Size = New System.Drawing.Size(74, 18)
        Me.LabelJoueur3.TabIndex = 0
        Me.LabelJoueur3.Text = "Joueur 3"
        '
        'PanelJoueur4
        '
        Me.PanelJoueur4.Controls.Add(Me.TextBoxAgejoueur4)
        Me.PanelJoueur4.Controls.Add(Me.TextBoxNomjoueur4)
        Me.PanelJoueur4.Controls.Add(Me.LabelJoueur4)
        Me.PanelJoueur4.Location = New System.Drawing.Point(12, 288)
        Me.PanelJoueur4.Name = "PanelJoueur4"
        Me.PanelJoueur4.Size = New System.Drawing.Size(810, 100)
        Me.PanelJoueur4.TabIndex = 11
        '
        'TextBoxAgejoueur4
        '
        Me.TextBoxAgejoueur4.Location = New System.Drawing.Point(82, 56)
        Me.TextBoxAgejoueur4.Name = "TextBoxAgejoueur4"
        Me.TextBoxAgejoueur4.Size = New System.Drawing.Size(197, 20)
        Me.TextBoxAgejoueur4.TabIndex = 2
        Me.TextBoxAgejoueur4.Text = "Age du joueur"
        '
        'TextBoxNomjoueur4
        '
        Me.TextBoxNomjoueur4.Location = New System.Drawing.Point(82, 30)
        Me.TextBoxNomjoueur4.Name = "TextBoxNomjoueur4"
        Me.TextBoxNomjoueur4.Size = New System.Drawing.Size(197, 20)
        Me.TextBoxNomjoueur4.TabIndex = 1
        Me.TextBoxNomjoueur4.Text = "Nom du joueur"
        '
        'LabelJoueur4
        '
        Me.LabelJoueur4.AutoSize = True
        Me.LabelJoueur4.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelJoueur4.Location = New System.Drawing.Point(79, 9)
        Me.LabelJoueur4.Name = "LabelJoueur4"
        Me.LabelJoueur4.Size = New System.Drawing.Size(74, 18)
        Me.LabelJoueur4.TabIndex = 0
        Me.LabelJoueur4.Text = "Joueur 4"
        '
        'PanelBoutonsAjouts
        '
        Me.PanelBoutonsAjouts.Controls.Add(Me.BouttonPlusUnJoueur)
        Me.PanelBoutonsAjouts.Controls.Add(Me.bouttonmoins)
        Me.PanelBoutonsAjouts.Location = New System.Drawing.Point(743, 438)
        Me.PanelBoutonsAjouts.Name = "PanelBoutonsAjouts"
        Me.PanelBoutonsAjouts.Size = New System.Drawing.Size(79, 32)
        Me.PanelBoutonsAjouts.TabIndex = 12
        '
        'FenetreParametrage
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(834, 482)
        Me.Controls.Add(Me.PanelBoutonsAjouts)
        Me.Controls.Add(Me.PanelJoueur4)
        Me.Controls.Add(Me.PanelJoueur3)
        Me.Controls.Add(Me.TextBoxAgejoueur2)
        Me.Controls.Add(Me.TextBoxNomjoueur2)
        Me.Controls.Add(Me.TextBoxAgejoueur1)
        Me.Controls.Add(Me.TextBoxNomjoueur1)
        Me.Controls.Add(Me.Labeljoueur2)
        Me.Controls.Add(Me.Labeljoueur1)
        Me.Controls.Add(Me.BouttonAnnuler)
        Me.Controls.Add(Me.BouttonDemarrer)
        Me.Name = "FenetreParametrage"
        Me.Text = "FenetreParametrage"
        Me.PanelJoueur3.ResumeLayout(False)
        Me.PanelJoueur3.PerformLayout()
        Me.PanelJoueur4.ResumeLayout(False)
        Me.PanelJoueur4.PerformLayout()
        Me.PanelBoutonsAjouts.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents BouttonPlusUnJoueur As Button
    Friend WithEvents bouttonmoins As Button
    Friend WithEvents BouttonDemarrer As Button
    Friend WithEvents BouttonAnnuler As Button
    Friend WithEvents Labeljoueur1 As Label
    Friend WithEvents Labeljoueur2 As Label
    Friend WithEvents TextBoxNomjoueur1 As TextBox
    Friend WithEvents TextBoxAgejoueur1 As TextBox
    Friend WithEvents TextBoxNomjoueur2 As TextBox
    Friend WithEvents TextBoxAgejoueur2 As TextBox
    Friend WithEvents PanelJoueur3 As Panel
    Friend WithEvents TextBoxAgejoueur3 As TextBox
    Friend WithEvents TextBoxNomjoueur3 As TextBox
    Friend WithEvents LabelJoueur3 As Label
    Friend WithEvents PanelJoueur4 As Panel
    Friend WithEvents TextBoxAgejoueur4 As TextBox
    Friend WithEvents TextBoxNomjoueur4 As TextBox
    Friend WithEvents LabelJoueur4 As Label
    Friend WithEvents PanelBoutonsAjouts As Panel
End Class
