﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.NomQwirkle = New System.Windows.Forms.Label()
        Me.ButtonJouer = New System.Windows.Forms.Button()
        Me.ButtonQuitter = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'NomQwirkle
        '
        Me.NomQwirkle.AutoSize = True
        Me.NomQwirkle.Font = New System.Drawing.Font("Microsoft Sans Serif", 48.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NomQwirkle.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.NomQwirkle.Location = New System.Drawing.Point(284, 53)
        Me.NomQwirkle.Name = "NomQwirkle"
        Me.NomQwirkle.Size = New System.Drawing.Size(252, 73)
        Me.NomQwirkle.TabIndex = 0
        Me.NomQwirkle.Text = "Qwirkle"
        '
        'ButtonJouer
        '
        Me.ButtonJouer.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonJouer.Location = New System.Drawing.Point(312, 233)
        Me.ButtonJouer.Name = "ButtonJouer"
        Me.ButtonJouer.Size = New System.Drawing.Size(188, 39)
        Me.ButtonJouer.TabIndex = 1
        Me.ButtonJouer.Text = "Jouer"
        Me.ButtonJouer.UseVisualStyleBackColor = True
        '
        'ButtonQuitter
        '
        Me.ButtonQuitter.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonQuitter.Location = New System.Drawing.Point(312, 309)
        Me.ButtonQuitter.Name = "ButtonQuitter"
        Me.ButtonQuitter.Size = New System.Drawing.Size(188, 39)
        Me.ButtonQuitter.TabIndex = 2
        Me.ButtonQuitter.Text = "Quitter"
        Me.ButtonQuitter.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(834, 482)
        Me.Controls.Add(Me.ButtonQuitter)
        Me.Controls.Add(Me.ButtonJouer)
        Me.Controls.Add(Me.NomQwirkle)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents NomQwirkle As Label
    Friend WithEvents ButtonJouer As Button
    Friend WithEvents ButtonQuitter As Button
End Class
