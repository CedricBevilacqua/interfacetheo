﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class PlateauQwirkle
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.L1C1 = New System.Windows.Forms.PictureBox()
        Me.L1C2 = New System.Windows.Forms.PictureBox()
        Me.L1C4 = New System.Windows.Forms.PictureBox()
        Me.L1C3 = New System.Windows.Forms.PictureBox()
        Me.L1C5 = New System.Windows.Forms.PictureBox()
        Me.L1C10 = New System.Windows.Forms.PictureBox()
        Me.L1C9 = New System.Windows.Forms.PictureBox()
        Me.L1C8 = New System.Windows.Forms.PictureBox()
        Me.L1C7 = New System.Windows.Forms.PictureBox()
        Me.L1C6 = New System.Windows.Forms.PictureBox()
        Me.L1C15 = New System.Windows.Forms.PictureBox()
        Me.L1C14 = New System.Windows.Forms.PictureBox()
        Me.L1C13 = New System.Windows.Forms.PictureBox()
        Me.L1C12 = New System.Windows.Forms.PictureBox()
        Me.L1C11 = New System.Windows.Forms.PictureBox()
        Me.L1C20 = New System.Windows.Forms.PictureBox()
        Me.L1C19 = New System.Windows.Forms.PictureBox()
        Me.L1C18 = New System.Windows.Forms.PictureBox()
        Me.L1C17 = New System.Windows.Forms.PictureBox()
        Me.L1C16 = New System.Windows.Forms.PictureBox()
        Me.L2C20 = New System.Windows.Forms.PictureBox()
        Me.L2C19 = New System.Windows.Forms.PictureBox()
        Me.L2C18 = New System.Windows.Forms.PictureBox()
        Me.L2C17 = New System.Windows.Forms.PictureBox()
        Me.L2C16 = New System.Windows.Forms.PictureBox()
        Me.L2C15 = New System.Windows.Forms.PictureBox()
        Me.L2C14 = New System.Windows.Forms.PictureBox()
        Me.L2C13 = New System.Windows.Forms.PictureBox()
        Me.L2C12 = New System.Windows.Forms.PictureBox()
        Me.L2C11 = New System.Windows.Forms.PictureBox()
        Me.L2C10 = New System.Windows.Forms.PictureBox()
        Me.L2C9 = New System.Windows.Forms.PictureBox()
        Me.L2C8 = New System.Windows.Forms.PictureBox()
        Me.L2C7 = New System.Windows.Forms.PictureBox()
        Me.L2C6 = New System.Windows.Forms.PictureBox()
        Me.L2C5 = New System.Windows.Forms.PictureBox()
        Me.L2C4 = New System.Windows.Forms.PictureBox()
        Me.L2C3 = New System.Windows.Forms.PictureBox()
        Me.L2C2 = New System.Windows.Forms.PictureBox()
        Me.L2C1 = New System.Windows.Forms.PictureBox()
        Me.L3C20 = New System.Windows.Forms.PictureBox()
        Me.L3C19 = New System.Windows.Forms.PictureBox()
        Me.L3C18 = New System.Windows.Forms.PictureBox()
        Me.L3C17 = New System.Windows.Forms.PictureBox()
        Me.L3C16 = New System.Windows.Forms.PictureBox()
        Me.L3C15 = New System.Windows.Forms.PictureBox()
        Me.L3C14 = New System.Windows.Forms.PictureBox()
        Me.L3C13 = New System.Windows.Forms.PictureBox()
        Me.L3C12 = New System.Windows.Forms.PictureBox()
        Me.L3C11 = New System.Windows.Forms.PictureBox()
        Me.L3C10 = New System.Windows.Forms.PictureBox()
        Me.L3C9 = New System.Windows.Forms.PictureBox()
        Me.L3C8 = New System.Windows.Forms.PictureBox()
        Me.L3C7 = New System.Windows.Forms.PictureBox()
        Me.L3C6 = New System.Windows.Forms.PictureBox()
        Me.L3C5 = New System.Windows.Forms.PictureBox()
        Me.L3C4 = New System.Windows.Forms.PictureBox()
        Me.L3C3 = New System.Windows.Forms.PictureBox()
        Me.L3C2 = New System.Windows.Forms.PictureBox()
        Me.L3C1 = New System.Windows.Forms.PictureBox()
        Me.L4C20 = New System.Windows.Forms.PictureBox()
        Me.L4C19 = New System.Windows.Forms.PictureBox()
        Me.L4C18 = New System.Windows.Forms.PictureBox()
        Me.L4C17 = New System.Windows.Forms.PictureBox()
        Me.L4C16 = New System.Windows.Forms.PictureBox()
        Me.L4C15 = New System.Windows.Forms.PictureBox()
        Me.L4C14 = New System.Windows.Forms.PictureBox()
        Me.L4C13 = New System.Windows.Forms.PictureBox()
        Me.L4C12 = New System.Windows.Forms.PictureBox()
        Me.L4C11 = New System.Windows.Forms.PictureBox()
        Me.L4C10 = New System.Windows.Forms.PictureBox()
        Me.L4C9 = New System.Windows.Forms.PictureBox()
        Me.L4C8 = New System.Windows.Forms.PictureBox()
        Me.L4C7 = New System.Windows.Forms.PictureBox()
        Me.L4C6 = New System.Windows.Forms.PictureBox()
        Me.L4C5 = New System.Windows.Forms.PictureBox()
        Me.L4C4 = New System.Windows.Forms.PictureBox()
        Me.L4C3 = New System.Windows.Forms.PictureBox()
        Me.L4C2 = New System.Windows.Forms.PictureBox()
        Me.L4C1 = New System.Windows.Forms.PictureBox()
        Me.L5C20 = New System.Windows.Forms.PictureBox()
        Me.L5C19 = New System.Windows.Forms.PictureBox()
        Me.L5C18 = New System.Windows.Forms.PictureBox()
        Me.L5C17 = New System.Windows.Forms.PictureBox()
        Me.L5C16 = New System.Windows.Forms.PictureBox()
        Me.L5C15 = New System.Windows.Forms.PictureBox()
        Me.L5C14 = New System.Windows.Forms.PictureBox()
        Me.L5C13 = New System.Windows.Forms.PictureBox()
        Me.L5C12 = New System.Windows.Forms.PictureBox()
        Me.L5C11 = New System.Windows.Forms.PictureBox()
        Me.L5C10 = New System.Windows.Forms.PictureBox()
        Me.L5C9 = New System.Windows.Forms.PictureBox()
        Me.L5C8 = New System.Windows.Forms.PictureBox()
        Me.L5C7 = New System.Windows.Forms.PictureBox()
        Me.L5C6 = New System.Windows.Forms.PictureBox()
        Me.L5C5 = New System.Windows.Forms.PictureBox()
        Me.L5C4 = New System.Windows.Forms.PictureBox()
        Me.L5C3 = New System.Windows.Forms.PictureBox()
        Me.L5C2 = New System.Windows.Forms.PictureBox()
        Me.L5C1 = New System.Windows.Forms.PictureBox()
        Me.L6C20 = New System.Windows.Forms.PictureBox()
        Me.L6C19 = New System.Windows.Forms.PictureBox()
        Me.L6C18 = New System.Windows.Forms.PictureBox()
        Me.L6C17 = New System.Windows.Forms.PictureBox()
        Me.L6C16 = New System.Windows.Forms.PictureBox()
        Me.L6C15 = New System.Windows.Forms.PictureBox()
        Me.L6C14 = New System.Windows.Forms.PictureBox()
        Me.L6C13 = New System.Windows.Forms.PictureBox()
        Me.L6C12 = New System.Windows.Forms.PictureBox()
        Me.L6C11 = New System.Windows.Forms.PictureBox()
        Me.L6C10 = New System.Windows.Forms.PictureBox()
        Me.L6C9 = New System.Windows.Forms.PictureBox()
        Me.L6C8 = New System.Windows.Forms.PictureBox()
        Me.L6C7 = New System.Windows.Forms.PictureBox()
        Me.L6C6 = New System.Windows.Forms.PictureBox()
        Me.L6C5 = New System.Windows.Forms.PictureBox()
        Me.L6C4 = New System.Windows.Forms.PictureBox()
        Me.L6C3 = New System.Windows.Forms.PictureBox()
        Me.L6C2 = New System.Windows.Forms.PictureBox()
        Me.L6C1 = New System.Windows.Forms.PictureBox()
        Me.L7C20 = New System.Windows.Forms.PictureBox()
        Me.L7C19 = New System.Windows.Forms.PictureBox()
        Me.L7C18 = New System.Windows.Forms.PictureBox()
        Me.L7C17 = New System.Windows.Forms.PictureBox()
        Me.L7C16 = New System.Windows.Forms.PictureBox()
        Me.L7C15 = New System.Windows.Forms.PictureBox()
        Me.L7C14 = New System.Windows.Forms.PictureBox()
        Me.L7C13 = New System.Windows.Forms.PictureBox()
        Me.L7C12 = New System.Windows.Forms.PictureBox()
        Me.L7C11 = New System.Windows.Forms.PictureBox()
        Me.L7C10 = New System.Windows.Forms.PictureBox()
        Me.L7C9 = New System.Windows.Forms.PictureBox()
        Me.L7C8 = New System.Windows.Forms.PictureBox()
        Me.L7C7 = New System.Windows.Forms.PictureBox()
        Me.L7C6 = New System.Windows.Forms.PictureBox()
        Me.L7C5 = New System.Windows.Forms.PictureBox()
        Me.L7C4 = New System.Windows.Forms.PictureBox()
        Me.L7C3 = New System.Windows.Forms.PictureBox()
        Me.L7C2 = New System.Windows.Forms.PictureBox()
        Me.L7C1 = New System.Windows.Forms.PictureBox()
        Me.L9C20 = New System.Windows.Forms.PictureBox()
        Me.L9C19 = New System.Windows.Forms.PictureBox()
        Me.L9C18 = New System.Windows.Forms.PictureBox()
        Me.L9C17 = New System.Windows.Forms.PictureBox()
        Me.L9C16 = New System.Windows.Forms.PictureBox()
        Me.L9C15 = New System.Windows.Forms.PictureBox()
        Me.L9C14 = New System.Windows.Forms.PictureBox()
        Me.L9C13 = New System.Windows.Forms.PictureBox()
        Me.L9C12 = New System.Windows.Forms.PictureBox()
        Me.L9C11 = New System.Windows.Forms.PictureBox()
        Me.L9C10 = New System.Windows.Forms.PictureBox()
        Me.L9C9 = New System.Windows.Forms.PictureBox()
        Me.L9C8 = New System.Windows.Forms.PictureBox()
        Me.L9C7 = New System.Windows.Forms.PictureBox()
        Me.L9C6 = New System.Windows.Forms.PictureBox()
        Me.L9C5 = New System.Windows.Forms.PictureBox()
        Me.L9C4 = New System.Windows.Forms.PictureBox()
        Me.L9C3 = New System.Windows.Forms.PictureBox()
        Me.L9C2 = New System.Windows.Forms.PictureBox()
        Me.L9C1 = New System.Windows.Forms.PictureBox()
        Me.L8C20 = New System.Windows.Forms.PictureBox()
        Me.L8C19 = New System.Windows.Forms.PictureBox()
        Me.L8C18 = New System.Windows.Forms.PictureBox()
        Me.L8C17 = New System.Windows.Forms.PictureBox()
        Me.L8C16 = New System.Windows.Forms.PictureBox()
        Me.L8C15 = New System.Windows.Forms.PictureBox()
        Me.L8C14 = New System.Windows.Forms.PictureBox()
        Me.L8C13 = New System.Windows.Forms.PictureBox()
        Me.L8C12 = New System.Windows.Forms.PictureBox()
        Me.L8C11 = New System.Windows.Forms.PictureBox()
        Me.L8C10 = New System.Windows.Forms.PictureBox()
        Me.L8C9 = New System.Windows.Forms.PictureBox()
        Me.L8C8 = New System.Windows.Forms.PictureBox()
        Me.L8C7 = New System.Windows.Forms.PictureBox()
        Me.L8C6 = New System.Windows.Forms.PictureBox()
        Me.L8C5 = New System.Windows.Forms.PictureBox()
        Me.L8C4 = New System.Windows.Forms.PictureBox()
        Me.L8C3 = New System.Windows.Forms.PictureBox()
        Me.L8C2 = New System.Windows.Forms.PictureBox()
        Me.L8C1 = New System.Windows.Forms.PictureBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.L10C19 = New System.Windows.Forms.PictureBox()
        Me.L10C18 = New System.Windows.Forms.PictureBox()
        Me.L10C17 = New System.Windows.Forms.PictureBox()
        Me.L10C16 = New System.Windows.Forms.PictureBox()
        Me.L10C15 = New System.Windows.Forms.PictureBox()
        Me.L10C14 = New System.Windows.Forms.PictureBox()
        Me.L10C13 = New System.Windows.Forms.PictureBox()
        Me.L10C12 = New System.Windows.Forms.PictureBox()
        Me.L10C11 = New System.Windows.Forms.PictureBox()
        Me.L10C10 = New System.Windows.Forms.PictureBox()
        Me.L10C9 = New System.Windows.Forms.PictureBox()
        Me.L10C8 = New System.Windows.Forms.PictureBox()
        Me.L10C7 = New System.Windows.Forms.PictureBox()
        Me.L10C6 = New System.Windows.Forms.PictureBox()
        Me.L10C5 = New System.Windows.Forms.PictureBox()
        Me.L10C4 = New System.Windows.Forms.PictureBox()
        Me.L10C3 = New System.Windows.Forms.PictureBox()
        Me.L10C2 = New System.Windows.Forms.PictureBox()
        Me.L10C1 = New System.Windows.Forms.PictureBox()
        Me.LabelNbtuilerestante = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Case1imagejoueur = New System.Windows.Forms.PictureBox()
        Me.Case6imagejoueur = New System.Windows.Forms.PictureBox()
        Me.Case5imagejoueur = New System.Windows.Forms.PictureBox()
        Me.Case4imagejoueur = New System.Windows.Forms.PictureBox()
        Me.Case3imagejoueur = New System.Windows.Forms.PictureBox()
        Me.Case2imagejoueur = New System.Windows.Forms.PictureBox()
        Me.ButtonAnnuler = New System.Windows.Forms.Button()
        Me.ButtonEchanger = New System.Windows.Forms.Button()
        Me.ButtonValider = New System.Windows.Forms.Button()
        Me.LabelNomjoueurencours = New System.Windows.Forms.Label()
        Me.Labelpointjoueurencours = New System.Windows.Forms.Label()
        Me.PictureBox27 = New System.Windows.Forms.PictureBox()
        Me.L10C20 = New System.Windows.Forms.PictureBox()
        Me.L11C20 = New System.Windows.Forms.PictureBox()
        Me.PictureBox30 = New System.Windows.Forms.PictureBox()
        Me.L11C19 = New System.Windows.Forms.PictureBox()
        Me.L11C18 = New System.Windows.Forms.PictureBox()
        Me.L11C17 = New System.Windows.Forms.PictureBox()
        Me.L11C16 = New System.Windows.Forms.PictureBox()
        Me.L11C15 = New System.Windows.Forms.PictureBox()
        Me.L11C14 = New System.Windows.Forms.PictureBox()
        Me.L11C13 = New System.Windows.Forms.PictureBox()
        Me.L11C12 = New System.Windows.Forms.PictureBox()
        Me.L11C11 = New System.Windows.Forms.PictureBox()
        Me.L11C10 = New System.Windows.Forms.PictureBox()
        Me.L11C9 = New System.Windows.Forms.PictureBox()
        Me.L11C8 = New System.Windows.Forms.PictureBox()
        Me.L11C7 = New System.Windows.Forms.PictureBox()
        Me.L11C6 = New System.Windows.Forms.PictureBox()
        Me.L11C5 = New System.Windows.Forms.PictureBox()
        Me.L11C4 = New System.Windows.Forms.PictureBox()
        Me.L11C3 = New System.Windows.Forms.PictureBox()
        Me.L11C2 = New System.Windows.Forms.PictureBox()
        Me.L11C1 = New System.Windows.Forms.PictureBox()
        Me.L12C20 = New System.Windows.Forms.PictureBox()
        Me.PictureBox51 = New System.Windows.Forms.PictureBox()
        Me.L12C19 = New System.Windows.Forms.PictureBox()
        Me.L12C18 = New System.Windows.Forms.PictureBox()
        Me.L12C17 = New System.Windows.Forms.PictureBox()
        Me.L12C16 = New System.Windows.Forms.PictureBox()
        Me.L12C15 = New System.Windows.Forms.PictureBox()
        Me.L12C14 = New System.Windows.Forms.PictureBox()
        Me.L12C13 = New System.Windows.Forms.PictureBox()
        Me.L12C12 = New System.Windows.Forms.PictureBox()
        Me.L12C11 = New System.Windows.Forms.PictureBox()
        Me.L12C10 = New System.Windows.Forms.PictureBox()
        Me.L12C9 = New System.Windows.Forms.PictureBox()
        Me.L12C8 = New System.Windows.Forms.PictureBox()
        Me.L12C7 = New System.Windows.Forms.PictureBox()
        Me.L12C6 = New System.Windows.Forms.PictureBox()
        Me.L12C5 = New System.Windows.Forms.PictureBox()
        Me.L12C4 = New System.Windows.Forms.PictureBox()
        Me.L12C3 = New System.Windows.Forms.PictureBox()
        Me.L12C2 = New System.Windows.Forms.PictureBox()
        Me.L12C1 = New System.Windows.Forms.PictureBox()
        Me.L13C20 = New System.Windows.Forms.PictureBox()
        Me.PictureBox72 = New System.Windows.Forms.PictureBox()
        Me.L13C19 = New System.Windows.Forms.PictureBox()
        Me.L13C18 = New System.Windows.Forms.PictureBox()
        Me.L13C17 = New System.Windows.Forms.PictureBox()
        Me.L13C16 = New System.Windows.Forms.PictureBox()
        Me.L13C15 = New System.Windows.Forms.PictureBox()
        Me.L13C14 = New System.Windows.Forms.PictureBox()
        Me.L13C13 = New System.Windows.Forms.PictureBox()
        Me.L13C12 = New System.Windows.Forms.PictureBox()
        Me.L13C11 = New System.Windows.Forms.PictureBox()
        Me.L13C10 = New System.Windows.Forms.PictureBox()
        Me.L13C9 = New System.Windows.Forms.PictureBox()
        Me.L13C8 = New System.Windows.Forms.PictureBox()
        Me.L13C7 = New System.Windows.Forms.PictureBox()
        Me.L13C6 = New System.Windows.Forms.PictureBox()
        Me.L13C5 = New System.Windows.Forms.PictureBox()
        Me.L13C4 = New System.Windows.Forms.PictureBox()
        Me.L13C3 = New System.Windows.Forms.PictureBox()
        Me.L13C2 = New System.Windows.Forms.PictureBox()
        Me.L13C1 = New System.Windows.Forms.PictureBox()
        CType(Me.L1C1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L1C2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L1C4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L1C3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L1C5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L1C10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L1C9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L1C8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L1C7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L1C6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L1C15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L1C14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L1C13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L1C12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L1C11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L1C20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L1C19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L1C18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L1C17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L1C16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L2C20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L2C19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L2C18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L2C17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L2C16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L2C15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L2C14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L2C13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L2C12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L2C11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L2C10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L2C9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L2C8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L2C7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L2C6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L2C5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L2C4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L2C3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L2C2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L2C1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L3C20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L3C19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L3C18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L3C17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L3C16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L3C15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L3C14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L3C13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L3C12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L3C11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L3C10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L3C9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L3C8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L3C7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L3C6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L3C5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L3C4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L3C3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L3C2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L3C1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L4C20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L4C19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L4C18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L4C17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L4C16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L4C15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L4C14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L4C13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L4C12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L4C11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L4C10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L4C9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L4C8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L4C7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L4C6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L4C5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L4C4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L4C3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L4C2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L4C1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L5C20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L5C19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L5C18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L5C17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L5C16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L5C15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L5C14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L5C13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L5C12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L5C11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L5C10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L5C9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L5C8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L5C7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L5C6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L5C5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L5C4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L5C3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L5C2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L5C1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L6C20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L6C19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L6C18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L6C17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L6C16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L6C15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L6C14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L6C13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L6C12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L6C11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L6C10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L6C9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L6C8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L6C7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L6C6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L6C5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L6C4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L6C3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L6C2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L6C1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L7C20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L7C19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L7C18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L7C17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L7C16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L7C15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L7C14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L7C13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L7C12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L7C11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L7C10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L7C9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L7C8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L7C7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L7C6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L7C5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L7C4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L7C3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L7C2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L7C1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L9C20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L9C19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L9C18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L9C17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L9C16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L9C15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L9C14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L9C13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L9C12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L9C11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L9C10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L9C9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L9C8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L9C7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L9C6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L9C5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L9C4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L9C3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L9C2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L9C1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L8C20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L8C19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L8C18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L8C17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L8C16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L8C15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L8C14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L8C13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L8C12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L8C11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L8C10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L8C9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L8C8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L8C7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L8C6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L8C5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L8C4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L8C3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L8C2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L8C1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L10C19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L10C18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L10C17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L10C16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L10C15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L10C14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L10C13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L10C12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L10C11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L10C10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L10C9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L10C8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L10C7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L10C6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L10C5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L10C4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L10C3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L10C2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L10C1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Case1imagejoueur, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Case6imagejoueur, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Case5imagejoueur, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Case4imagejoueur, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Case3imagejoueur, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Case2imagejoueur, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox27, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L10C20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L11C20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox30, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L11C19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L11C18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L11C17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L11C16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L11C15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L11C14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L11C13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L11C12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L11C11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L11C10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L11C9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L11C8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L11C7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L11C6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L11C5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L11C4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L11C3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L11C2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L11C1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L12C20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox51, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L12C19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L12C18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L12C17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L12C16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L12C15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L12C14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L12C13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L12C12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L12C11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L12C10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L12C9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L12C8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L12C7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L12C6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L12C5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L12C4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L12C3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L12C2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L12C1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L13C20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox72, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L13C19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L13C18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L13C17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L13C16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L13C15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L13C14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L13C13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L13C12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L13C11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L13C10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L13C9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L13C8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L13C7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L13C6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L13C5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L13C4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L13C3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L13C2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.L13C1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'L1C1
        '
        Me.L1C1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L1C1.Image = Global.interface_projet.My.Resources.Resources.CarreBleu
        Me.L1C1.Location = New System.Drawing.Point(107, 159)
        Me.L1C1.Margin = New System.Windows.Forms.Padding(0)
        Me.L1C1.Name = "L1C1"
        Me.L1C1.Size = New System.Drawing.Size(49, 50)
        Me.L1C1.TabIndex = 0
        Me.L1C1.TabStop = False
        '
        'L1C2
        '
        Me.L1C2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L1C2.Location = New System.Drawing.Point(58, 9)
        Me.L1C2.Margin = New System.Windows.Forms.Padding(0)
        Me.L1C2.Name = "L1C2"
        Me.L1C2.Size = New System.Drawing.Size(49, 50)
        Me.L1C2.TabIndex = 21
        Me.L1C2.TabStop = False
        '
        'L1C4
        '
        Me.L1C4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L1C4.Location = New System.Drawing.Point(156, 9)
        Me.L1C4.Margin = New System.Windows.Forms.Padding(0)
        Me.L1C4.Name = "L1C4"
        Me.L1C4.Size = New System.Drawing.Size(49, 50)
        Me.L1C4.TabIndex = 33
        Me.L1C4.TabStop = False
        '
        'L1C3
        '
        Me.L1C3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L1C3.Location = New System.Drawing.Point(107, 9)
        Me.L1C3.Margin = New System.Windows.Forms.Padding(0)
        Me.L1C3.Name = "L1C3"
        Me.L1C3.Size = New System.Drawing.Size(49, 50)
        Me.L1C3.TabIndex = 32
        Me.L1C3.TabStop = False
        '
        'L1C5
        '
        Me.L1C5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L1C5.Location = New System.Drawing.Point(205, 9)
        Me.L1C5.Margin = New System.Windows.Forms.Padding(0)
        Me.L1C5.Name = "L1C5"
        Me.L1C5.Size = New System.Drawing.Size(49, 50)
        Me.L1C5.TabIndex = 34
        Me.L1C5.TabStop = False
        '
        'L1C10
        '
        Me.L1C10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L1C10.Location = New System.Drawing.Point(450, 9)
        Me.L1C10.Margin = New System.Windows.Forms.Padding(0)
        Me.L1C10.Name = "L1C10"
        Me.L1C10.Size = New System.Drawing.Size(49, 50)
        Me.L1C10.TabIndex = 54
        Me.L1C10.TabStop = False
        '
        'L1C9
        '
        Me.L1C9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L1C9.Location = New System.Drawing.Point(401, 9)
        Me.L1C9.Margin = New System.Windows.Forms.Padding(0)
        Me.L1C9.Name = "L1C9"
        Me.L1C9.Size = New System.Drawing.Size(49, 50)
        Me.L1C9.TabIndex = 53
        Me.L1C9.TabStop = False
        '
        'L1C8
        '
        Me.L1C8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L1C8.Location = New System.Drawing.Point(352, 9)
        Me.L1C8.Margin = New System.Windows.Forms.Padding(0)
        Me.L1C8.Name = "L1C8"
        Me.L1C8.Size = New System.Drawing.Size(49, 50)
        Me.L1C8.TabIndex = 52
        Me.L1C8.TabStop = False
        '
        'L1C7
        '
        Me.L1C7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L1C7.Location = New System.Drawing.Point(303, 9)
        Me.L1C7.Margin = New System.Windows.Forms.Padding(0)
        Me.L1C7.Name = "L1C7"
        Me.L1C7.Size = New System.Drawing.Size(49, 50)
        Me.L1C7.TabIndex = 51
        Me.L1C7.TabStop = False
        '
        'L1C6
        '
        Me.L1C6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L1C6.Location = New System.Drawing.Point(254, 9)
        Me.L1C6.Margin = New System.Windows.Forms.Padding(0)
        Me.L1C6.Name = "L1C6"
        Me.L1C6.Size = New System.Drawing.Size(49, 50)
        Me.L1C6.TabIndex = 50
        Me.L1C6.TabStop = False
        '
        'L1C15
        '
        Me.L1C15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L1C15.Location = New System.Drawing.Point(695, 9)
        Me.L1C15.Margin = New System.Windows.Forms.Padding(0)
        Me.L1C15.Name = "L1C15"
        Me.L1C15.Size = New System.Drawing.Size(49, 50)
        Me.L1C15.TabIndex = 59
        Me.L1C15.TabStop = False
        '
        'L1C14
        '
        Me.L1C14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L1C14.Location = New System.Drawing.Point(646, 9)
        Me.L1C14.Margin = New System.Windows.Forms.Padding(0)
        Me.L1C14.Name = "L1C14"
        Me.L1C14.Size = New System.Drawing.Size(49, 50)
        Me.L1C14.TabIndex = 58
        Me.L1C14.TabStop = False
        '
        'L1C13
        '
        Me.L1C13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L1C13.Location = New System.Drawing.Point(597, 9)
        Me.L1C13.Margin = New System.Windows.Forms.Padding(0)
        Me.L1C13.Name = "L1C13"
        Me.L1C13.Size = New System.Drawing.Size(49, 50)
        Me.L1C13.TabIndex = 57
        Me.L1C13.TabStop = False
        '
        'L1C12
        '
        Me.L1C12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L1C12.Location = New System.Drawing.Point(548, 9)
        Me.L1C12.Margin = New System.Windows.Forms.Padding(0)
        Me.L1C12.Name = "L1C12"
        Me.L1C12.Size = New System.Drawing.Size(49, 50)
        Me.L1C12.TabIndex = 56
        Me.L1C12.TabStop = False
        '
        'L1C11
        '
        Me.L1C11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L1C11.Location = New System.Drawing.Point(499, 9)
        Me.L1C11.Margin = New System.Windows.Forms.Padding(0)
        Me.L1C11.Name = "L1C11"
        Me.L1C11.Size = New System.Drawing.Size(49, 50)
        Me.L1C11.TabIndex = 55
        Me.L1C11.TabStop = False
        '
        'L1C20
        '
        Me.L1C20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L1C20.Location = New System.Drawing.Point(940, 9)
        Me.L1C20.Margin = New System.Windows.Forms.Padding(0)
        Me.L1C20.Name = "L1C20"
        Me.L1C20.Size = New System.Drawing.Size(49, 50)
        Me.L1C20.TabIndex = 64
        Me.L1C20.TabStop = False
        '
        'L1C19
        '
        Me.L1C19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L1C19.Location = New System.Drawing.Point(891, 9)
        Me.L1C19.Margin = New System.Windows.Forms.Padding(0)
        Me.L1C19.Name = "L1C19"
        Me.L1C19.Size = New System.Drawing.Size(49, 50)
        Me.L1C19.TabIndex = 63
        Me.L1C19.TabStop = False
        '
        'L1C18
        '
        Me.L1C18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L1C18.Location = New System.Drawing.Point(842, 9)
        Me.L1C18.Margin = New System.Windows.Forms.Padding(0)
        Me.L1C18.Name = "L1C18"
        Me.L1C18.Size = New System.Drawing.Size(49, 50)
        Me.L1C18.TabIndex = 62
        Me.L1C18.TabStop = False
        '
        'L1C17
        '
        Me.L1C17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L1C17.Location = New System.Drawing.Point(793, 9)
        Me.L1C17.Margin = New System.Windows.Forms.Padding(0)
        Me.L1C17.Name = "L1C17"
        Me.L1C17.Size = New System.Drawing.Size(49, 50)
        Me.L1C17.TabIndex = 61
        Me.L1C17.TabStop = False
        '
        'L1C16
        '
        Me.L1C16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L1C16.Location = New System.Drawing.Point(744, 9)
        Me.L1C16.Margin = New System.Windows.Forms.Padding(0)
        Me.L1C16.Name = "L1C16"
        Me.L1C16.Size = New System.Drawing.Size(49, 50)
        Me.L1C16.TabIndex = 60
        Me.L1C16.TabStop = False
        '
        'L2C20
        '
        Me.L2C20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L2C20.Location = New System.Drawing.Point(940, 59)
        Me.L2C20.Margin = New System.Windows.Forms.Padding(0)
        Me.L2C20.Name = "L2C20"
        Me.L2C20.Size = New System.Drawing.Size(49, 50)
        Me.L2C20.TabIndex = 84
        Me.L2C20.TabStop = False
        '
        'L2C19
        '
        Me.L2C19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L2C19.Location = New System.Drawing.Point(891, 59)
        Me.L2C19.Margin = New System.Windows.Forms.Padding(0)
        Me.L2C19.Name = "L2C19"
        Me.L2C19.Size = New System.Drawing.Size(49, 50)
        Me.L2C19.TabIndex = 83
        Me.L2C19.TabStop = False
        '
        'L2C18
        '
        Me.L2C18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L2C18.Location = New System.Drawing.Point(842, 59)
        Me.L2C18.Margin = New System.Windows.Forms.Padding(0)
        Me.L2C18.Name = "L2C18"
        Me.L2C18.Size = New System.Drawing.Size(49, 50)
        Me.L2C18.TabIndex = 82
        Me.L2C18.TabStop = False
        '
        'L2C17
        '
        Me.L2C17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L2C17.Location = New System.Drawing.Point(793, 59)
        Me.L2C17.Margin = New System.Windows.Forms.Padding(0)
        Me.L2C17.Name = "L2C17"
        Me.L2C17.Size = New System.Drawing.Size(49, 50)
        Me.L2C17.TabIndex = 81
        Me.L2C17.TabStop = False
        '
        'L2C16
        '
        Me.L2C16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L2C16.Location = New System.Drawing.Point(744, 59)
        Me.L2C16.Margin = New System.Windows.Forms.Padding(0)
        Me.L2C16.Name = "L2C16"
        Me.L2C16.Size = New System.Drawing.Size(49, 50)
        Me.L2C16.TabIndex = 80
        Me.L2C16.TabStop = False
        '
        'L2C15
        '
        Me.L2C15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L2C15.Location = New System.Drawing.Point(695, 59)
        Me.L2C15.Margin = New System.Windows.Forms.Padding(0)
        Me.L2C15.Name = "L2C15"
        Me.L2C15.Size = New System.Drawing.Size(49, 50)
        Me.L2C15.TabIndex = 79
        Me.L2C15.TabStop = False
        '
        'L2C14
        '
        Me.L2C14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L2C14.Location = New System.Drawing.Point(646, 59)
        Me.L2C14.Margin = New System.Windows.Forms.Padding(0)
        Me.L2C14.Name = "L2C14"
        Me.L2C14.Size = New System.Drawing.Size(49, 50)
        Me.L2C14.TabIndex = 78
        Me.L2C14.TabStop = False
        '
        'L2C13
        '
        Me.L2C13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L2C13.Location = New System.Drawing.Point(597, 59)
        Me.L2C13.Margin = New System.Windows.Forms.Padding(0)
        Me.L2C13.Name = "L2C13"
        Me.L2C13.Size = New System.Drawing.Size(49, 50)
        Me.L2C13.TabIndex = 77
        Me.L2C13.TabStop = False
        '
        'L2C12
        '
        Me.L2C12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L2C12.Location = New System.Drawing.Point(548, 59)
        Me.L2C12.Margin = New System.Windows.Forms.Padding(0)
        Me.L2C12.Name = "L2C12"
        Me.L2C12.Size = New System.Drawing.Size(49, 50)
        Me.L2C12.TabIndex = 76
        Me.L2C12.TabStop = False
        '
        'L2C11
        '
        Me.L2C11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L2C11.Location = New System.Drawing.Point(499, 59)
        Me.L2C11.Margin = New System.Windows.Forms.Padding(0)
        Me.L2C11.Name = "L2C11"
        Me.L2C11.Size = New System.Drawing.Size(49, 50)
        Me.L2C11.TabIndex = 75
        Me.L2C11.TabStop = False
        '
        'L2C10
        '
        Me.L2C10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L2C10.Location = New System.Drawing.Point(450, 59)
        Me.L2C10.Margin = New System.Windows.Forms.Padding(0)
        Me.L2C10.Name = "L2C10"
        Me.L2C10.Size = New System.Drawing.Size(49, 50)
        Me.L2C10.TabIndex = 74
        Me.L2C10.TabStop = False
        '
        'L2C9
        '
        Me.L2C9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L2C9.Location = New System.Drawing.Point(401, 59)
        Me.L2C9.Margin = New System.Windows.Forms.Padding(0)
        Me.L2C9.Name = "L2C9"
        Me.L2C9.Size = New System.Drawing.Size(49, 50)
        Me.L2C9.TabIndex = 73
        Me.L2C9.TabStop = False
        '
        'L2C8
        '
        Me.L2C8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L2C8.Location = New System.Drawing.Point(352, 59)
        Me.L2C8.Margin = New System.Windows.Forms.Padding(0)
        Me.L2C8.Name = "L2C8"
        Me.L2C8.Size = New System.Drawing.Size(49, 50)
        Me.L2C8.TabIndex = 72
        Me.L2C8.TabStop = False
        '
        'L2C7
        '
        Me.L2C7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L2C7.Location = New System.Drawing.Point(303, 59)
        Me.L2C7.Margin = New System.Windows.Forms.Padding(0)
        Me.L2C7.Name = "L2C7"
        Me.L2C7.Size = New System.Drawing.Size(49, 50)
        Me.L2C7.TabIndex = 71
        Me.L2C7.TabStop = False
        '
        'L2C6
        '
        Me.L2C6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L2C6.Location = New System.Drawing.Point(254, 59)
        Me.L2C6.Margin = New System.Windows.Forms.Padding(0)
        Me.L2C6.Name = "L2C6"
        Me.L2C6.Size = New System.Drawing.Size(49, 50)
        Me.L2C6.TabIndex = 70
        Me.L2C6.TabStop = False
        '
        'L2C5
        '
        Me.L2C5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L2C5.Location = New System.Drawing.Point(205, 59)
        Me.L2C5.Margin = New System.Windows.Forms.Padding(0)
        Me.L2C5.Name = "L2C5"
        Me.L2C5.Size = New System.Drawing.Size(49, 50)
        Me.L2C5.TabIndex = 69
        Me.L2C5.TabStop = False
        '
        'L2C4
        '
        Me.L2C4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L2C4.Location = New System.Drawing.Point(156, 59)
        Me.L2C4.Margin = New System.Windows.Forms.Padding(0)
        Me.L2C4.Name = "L2C4"
        Me.L2C4.Size = New System.Drawing.Size(49, 50)
        Me.L2C4.TabIndex = 68
        Me.L2C4.TabStop = False
        '
        'L2C3
        '
        Me.L2C3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L2C3.Location = New System.Drawing.Point(107, 59)
        Me.L2C3.Margin = New System.Windows.Forms.Padding(0)
        Me.L2C3.Name = "L2C3"
        Me.L2C3.Size = New System.Drawing.Size(49, 50)
        Me.L2C3.TabIndex = 67
        Me.L2C3.TabStop = False
        '
        'L2C2
        '
        Me.L2C2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L2C2.Location = New System.Drawing.Point(58, 59)
        Me.L2C2.Margin = New System.Windows.Forms.Padding(0)
        Me.L2C2.Name = "L2C2"
        Me.L2C2.Size = New System.Drawing.Size(49, 50)
        Me.L2C2.TabIndex = 66
        Me.L2C2.TabStop = False
        '
        'L2C1
        '
        Me.L2C1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L2C1.Location = New System.Drawing.Point(9, 59)
        Me.L2C1.Margin = New System.Windows.Forms.Padding(0)
        Me.L2C1.Name = "L2C1"
        Me.L2C1.Size = New System.Drawing.Size(49, 50)
        Me.L2C1.TabIndex = 65
        Me.L2C1.TabStop = False
        '
        'L3C20
        '
        Me.L3C20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L3C20.Location = New System.Drawing.Point(940, 109)
        Me.L3C20.Margin = New System.Windows.Forms.Padding(0)
        Me.L3C20.Name = "L3C20"
        Me.L3C20.Size = New System.Drawing.Size(49, 50)
        Me.L3C20.TabIndex = 104
        Me.L3C20.TabStop = False
        '
        'L3C19
        '
        Me.L3C19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L3C19.Location = New System.Drawing.Point(891, 109)
        Me.L3C19.Margin = New System.Windows.Forms.Padding(0)
        Me.L3C19.Name = "L3C19"
        Me.L3C19.Size = New System.Drawing.Size(49, 50)
        Me.L3C19.TabIndex = 103
        Me.L3C19.TabStop = False
        '
        'L3C18
        '
        Me.L3C18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L3C18.Location = New System.Drawing.Point(842, 109)
        Me.L3C18.Margin = New System.Windows.Forms.Padding(0)
        Me.L3C18.Name = "L3C18"
        Me.L3C18.Size = New System.Drawing.Size(49, 50)
        Me.L3C18.TabIndex = 102
        Me.L3C18.TabStop = False
        '
        'L3C17
        '
        Me.L3C17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L3C17.Location = New System.Drawing.Point(793, 109)
        Me.L3C17.Margin = New System.Windows.Forms.Padding(0)
        Me.L3C17.Name = "L3C17"
        Me.L3C17.Size = New System.Drawing.Size(49, 50)
        Me.L3C17.TabIndex = 101
        Me.L3C17.TabStop = False
        '
        'L3C16
        '
        Me.L3C16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L3C16.Location = New System.Drawing.Point(744, 109)
        Me.L3C16.Margin = New System.Windows.Forms.Padding(0)
        Me.L3C16.Name = "L3C16"
        Me.L3C16.Size = New System.Drawing.Size(49, 50)
        Me.L3C16.TabIndex = 100
        Me.L3C16.TabStop = False
        '
        'L3C15
        '
        Me.L3C15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L3C15.Location = New System.Drawing.Point(695, 109)
        Me.L3C15.Margin = New System.Windows.Forms.Padding(0)
        Me.L3C15.Name = "L3C15"
        Me.L3C15.Size = New System.Drawing.Size(49, 50)
        Me.L3C15.TabIndex = 99
        Me.L3C15.TabStop = False
        '
        'L3C14
        '
        Me.L3C14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L3C14.Location = New System.Drawing.Point(646, 109)
        Me.L3C14.Margin = New System.Windows.Forms.Padding(0)
        Me.L3C14.Name = "L3C14"
        Me.L3C14.Size = New System.Drawing.Size(49, 50)
        Me.L3C14.TabIndex = 98
        Me.L3C14.TabStop = False
        '
        'L3C13
        '
        Me.L3C13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L3C13.Location = New System.Drawing.Point(597, 109)
        Me.L3C13.Margin = New System.Windows.Forms.Padding(0)
        Me.L3C13.Name = "L3C13"
        Me.L3C13.Size = New System.Drawing.Size(49, 50)
        Me.L3C13.TabIndex = 97
        Me.L3C13.TabStop = False
        '
        'L3C12
        '
        Me.L3C12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L3C12.Location = New System.Drawing.Point(548, 109)
        Me.L3C12.Margin = New System.Windows.Forms.Padding(0)
        Me.L3C12.Name = "L3C12"
        Me.L3C12.Size = New System.Drawing.Size(49, 50)
        Me.L3C12.TabIndex = 96
        Me.L3C12.TabStop = False
        '
        'L3C11
        '
        Me.L3C11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L3C11.Location = New System.Drawing.Point(499, 109)
        Me.L3C11.Margin = New System.Windows.Forms.Padding(0)
        Me.L3C11.Name = "L3C11"
        Me.L3C11.Size = New System.Drawing.Size(49, 50)
        Me.L3C11.TabIndex = 95
        Me.L3C11.TabStop = False
        '
        'L3C10
        '
        Me.L3C10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L3C10.Location = New System.Drawing.Point(450, 109)
        Me.L3C10.Margin = New System.Windows.Forms.Padding(0)
        Me.L3C10.Name = "L3C10"
        Me.L3C10.Size = New System.Drawing.Size(49, 50)
        Me.L3C10.TabIndex = 94
        Me.L3C10.TabStop = False
        '
        'L3C9
        '
        Me.L3C9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L3C9.Location = New System.Drawing.Point(401, 109)
        Me.L3C9.Margin = New System.Windows.Forms.Padding(0)
        Me.L3C9.Name = "L3C9"
        Me.L3C9.Size = New System.Drawing.Size(49, 50)
        Me.L3C9.TabIndex = 93
        Me.L3C9.TabStop = False
        '
        'L3C8
        '
        Me.L3C8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L3C8.Location = New System.Drawing.Point(352, 109)
        Me.L3C8.Margin = New System.Windows.Forms.Padding(0)
        Me.L3C8.Name = "L3C8"
        Me.L3C8.Size = New System.Drawing.Size(49, 50)
        Me.L3C8.TabIndex = 92
        Me.L3C8.TabStop = False
        '
        'L3C7
        '
        Me.L3C7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L3C7.Location = New System.Drawing.Point(303, 109)
        Me.L3C7.Margin = New System.Windows.Forms.Padding(0)
        Me.L3C7.Name = "L3C7"
        Me.L3C7.Size = New System.Drawing.Size(49, 50)
        Me.L3C7.TabIndex = 91
        Me.L3C7.TabStop = False
        '
        'L3C6
        '
        Me.L3C6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L3C6.Image = Global.interface_projet.My.Resources.Resources.CarreBleu
        Me.L3C6.Location = New System.Drawing.Point(254, 109)
        Me.L3C6.Margin = New System.Windows.Forms.Padding(0)
        Me.L3C6.Name = "L3C6"
        Me.L3C6.Size = New System.Drawing.Size(49, 50)
        Me.L3C6.TabIndex = 90
        Me.L3C6.TabStop = False
        '
        'L3C5
        '
        Me.L3C5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L3C5.Location = New System.Drawing.Point(205, 109)
        Me.L3C5.Margin = New System.Windows.Forms.Padding(0)
        Me.L3C5.Name = "L3C5"
        Me.L3C5.Size = New System.Drawing.Size(49, 50)
        Me.L3C5.TabIndex = 89
        Me.L3C5.TabStop = False
        '
        'L3C4
        '
        Me.L3C4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L3C4.Location = New System.Drawing.Point(156, 109)
        Me.L3C4.Margin = New System.Windows.Forms.Padding(0)
        Me.L3C4.Name = "L3C4"
        Me.L3C4.Size = New System.Drawing.Size(49, 50)
        Me.L3C4.TabIndex = 88
        Me.L3C4.TabStop = False
        '
        'L3C3
        '
        Me.L3C3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L3C3.Location = New System.Drawing.Point(107, 109)
        Me.L3C3.Margin = New System.Windows.Forms.Padding(0)
        Me.L3C3.Name = "L3C3"
        Me.L3C3.Size = New System.Drawing.Size(49, 50)
        Me.L3C3.TabIndex = 87
        Me.L3C3.TabStop = False
        '
        'L3C2
        '
        Me.L3C2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L3C2.Location = New System.Drawing.Point(58, 109)
        Me.L3C2.Margin = New System.Windows.Forms.Padding(0)
        Me.L3C2.Name = "L3C2"
        Me.L3C2.Size = New System.Drawing.Size(49, 50)
        Me.L3C2.TabIndex = 86
        Me.L3C2.TabStop = False
        '
        'L3C1
        '
        Me.L3C1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L3C1.Location = New System.Drawing.Point(9, 109)
        Me.L3C1.Margin = New System.Windows.Forms.Padding(0)
        Me.L3C1.Name = "L3C1"
        Me.L3C1.Size = New System.Drawing.Size(49, 50)
        Me.L3C1.TabIndex = 85
        Me.L3C1.TabStop = False
        '
        'L4C20
        '
        Me.L4C20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L4C20.Location = New System.Drawing.Point(940, 159)
        Me.L4C20.Margin = New System.Windows.Forms.Padding(0)
        Me.L4C20.Name = "L4C20"
        Me.L4C20.Size = New System.Drawing.Size(49, 50)
        Me.L4C20.TabIndex = 124
        Me.L4C20.TabStop = False
        '
        'L4C19
        '
        Me.L4C19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L4C19.Location = New System.Drawing.Point(891, 159)
        Me.L4C19.Margin = New System.Windows.Forms.Padding(0)
        Me.L4C19.Name = "L4C19"
        Me.L4C19.Size = New System.Drawing.Size(49, 50)
        Me.L4C19.TabIndex = 123
        Me.L4C19.TabStop = False
        '
        'L4C18
        '
        Me.L4C18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L4C18.Location = New System.Drawing.Point(842, 159)
        Me.L4C18.Margin = New System.Windows.Forms.Padding(0)
        Me.L4C18.Name = "L4C18"
        Me.L4C18.Size = New System.Drawing.Size(49, 50)
        Me.L4C18.TabIndex = 122
        Me.L4C18.TabStop = False
        '
        'L4C17
        '
        Me.L4C17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L4C17.Location = New System.Drawing.Point(793, 159)
        Me.L4C17.Margin = New System.Windows.Forms.Padding(0)
        Me.L4C17.Name = "L4C17"
        Me.L4C17.Size = New System.Drawing.Size(49, 50)
        Me.L4C17.TabIndex = 121
        Me.L4C17.TabStop = False
        '
        'L4C16
        '
        Me.L4C16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L4C16.Location = New System.Drawing.Point(744, 159)
        Me.L4C16.Margin = New System.Windows.Forms.Padding(0)
        Me.L4C16.Name = "L4C16"
        Me.L4C16.Size = New System.Drawing.Size(49, 50)
        Me.L4C16.TabIndex = 120
        Me.L4C16.TabStop = False
        '
        'L4C15
        '
        Me.L4C15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L4C15.Location = New System.Drawing.Point(695, 159)
        Me.L4C15.Margin = New System.Windows.Forms.Padding(0)
        Me.L4C15.Name = "L4C15"
        Me.L4C15.Size = New System.Drawing.Size(49, 50)
        Me.L4C15.TabIndex = 119
        Me.L4C15.TabStop = False
        '
        'L4C14
        '
        Me.L4C14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L4C14.Location = New System.Drawing.Point(646, 159)
        Me.L4C14.Margin = New System.Windows.Forms.Padding(0)
        Me.L4C14.Name = "L4C14"
        Me.L4C14.Size = New System.Drawing.Size(49, 50)
        Me.L4C14.TabIndex = 118
        Me.L4C14.TabStop = False
        '
        'L4C13
        '
        Me.L4C13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L4C13.Location = New System.Drawing.Point(597, 159)
        Me.L4C13.Margin = New System.Windows.Forms.Padding(0)
        Me.L4C13.Name = "L4C13"
        Me.L4C13.Size = New System.Drawing.Size(49, 50)
        Me.L4C13.TabIndex = 117
        Me.L4C13.TabStop = False
        '
        'L4C12
        '
        Me.L4C12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L4C12.Location = New System.Drawing.Point(548, 159)
        Me.L4C12.Margin = New System.Windows.Forms.Padding(0)
        Me.L4C12.Name = "L4C12"
        Me.L4C12.Size = New System.Drawing.Size(49, 50)
        Me.L4C12.TabIndex = 116
        Me.L4C12.TabStop = False
        '
        'L4C11
        '
        Me.L4C11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L4C11.Location = New System.Drawing.Point(499, 159)
        Me.L4C11.Margin = New System.Windows.Forms.Padding(0)
        Me.L4C11.Name = "L4C11"
        Me.L4C11.Size = New System.Drawing.Size(49, 50)
        Me.L4C11.TabIndex = 115
        Me.L4C11.TabStop = False
        '
        'L4C10
        '
        Me.L4C10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L4C10.Location = New System.Drawing.Point(450, 159)
        Me.L4C10.Margin = New System.Windows.Forms.Padding(0)
        Me.L4C10.Name = "L4C10"
        Me.L4C10.Size = New System.Drawing.Size(49, 50)
        Me.L4C10.TabIndex = 114
        Me.L4C10.TabStop = False
        '
        'L4C9
        '
        Me.L4C9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L4C9.Location = New System.Drawing.Point(401, 159)
        Me.L4C9.Margin = New System.Windows.Forms.Padding(0)
        Me.L4C9.Name = "L4C9"
        Me.L4C9.Size = New System.Drawing.Size(49, 50)
        Me.L4C9.TabIndex = 113
        Me.L4C9.TabStop = False
        '
        'L4C8
        '
        Me.L4C8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L4C8.Location = New System.Drawing.Point(352, 159)
        Me.L4C8.Margin = New System.Windows.Forms.Padding(0)
        Me.L4C8.Name = "L4C8"
        Me.L4C8.Size = New System.Drawing.Size(49, 50)
        Me.L4C8.TabIndex = 112
        Me.L4C8.TabStop = False
        '
        'L4C7
        '
        Me.L4C7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L4C7.Location = New System.Drawing.Point(303, 159)
        Me.L4C7.Margin = New System.Windows.Forms.Padding(0)
        Me.L4C7.Name = "L4C7"
        Me.L4C7.Size = New System.Drawing.Size(49, 50)
        Me.L4C7.TabIndex = 111
        Me.L4C7.TabStop = False
        '
        'L4C6
        '
        Me.L4C6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L4C6.Location = New System.Drawing.Point(254, 159)
        Me.L4C6.Margin = New System.Windows.Forms.Padding(0)
        Me.L4C6.Name = "L4C6"
        Me.L4C6.Size = New System.Drawing.Size(49, 50)
        Me.L4C6.TabIndex = 110
        Me.L4C6.TabStop = False
        '
        'L4C5
        '
        Me.L4C5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L4C5.Location = New System.Drawing.Point(205, 159)
        Me.L4C5.Margin = New System.Windows.Forms.Padding(0)
        Me.L4C5.Name = "L4C5"
        Me.L4C5.Size = New System.Drawing.Size(49, 50)
        Me.L4C5.TabIndex = 109
        Me.L4C5.TabStop = False
        '
        'L4C4
        '
        Me.L4C4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L4C4.Location = New System.Drawing.Point(156, 159)
        Me.L4C4.Margin = New System.Windows.Forms.Padding(0)
        Me.L4C4.Name = "L4C4"
        Me.L4C4.Size = New System.Drawing.Size(49, 50)
        Me.L4C4.TabIndex = 108
        Me.L4C4.TabStop = False
        '
        'L4C3
        '
        Me.L4C3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L4C3.Location = New System.Drawing.Point(9, 9)
        Me.L4C3.Margin = New System.Windows.Forms.Padding(0)
        Me.L4C3.Name = "L4C3"
        Me.L4C3.Size = New System.Drawing.Size(49, 50)
        Me.L4C3.TabIndex = 107
        Me.L4C3.TabStop = False
        '
        'L4C2
        '
        Me.L4C2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L4C2.Location = New System.Drawing.Point(58, 159)
        Me.L4C2.Margin = New System.Windows.Forms.Padding(0)
        Me.L4C2.Name = "L4C2"
        Me.L4C2.Size = New System.Drawing.Size(49, 50)
        Me.L4C2.TabIndex = 106
        Me.L4C2.TabStop = False
        '
        'L4C1
        '
        Me.L4C1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L4C1.Location = New System.Drawing.Point(9, 159)
        Me.L4C1.Margin = New System.Windows.Forms.Padding(0)
        Me.L4C1.Name = "L4C1"
        Me.L4C1.Size = New System.Drawing.Size(49, 50)
        Me.L4C1.TabIndex = 105
        Me.L4C1.TabStop = False
        '
        'L5C20
        '
        Me.L5C20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L5C20.Location = New System.Drawing.Point(940, 209)
        Me.L5C20.Margin = New System.Windows.Forms.Padding(0)
        Me.L5C20.Name = "L5C20"
        Me.L5C20.Size = New System.Drawing.Size(49, 50)
        Me.L5C20.TabIndex = 144
        Me.L5C20.TabStop = False
        '
        'L5C19
        '
        Me.L5C19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L5C19.Location = New System.Drawing.Point(891, 209)
        Me.L5C19.Margin = New System.Windows.Forms.Padding(0)
        Me.L5C19.Name = "L5C19"
        Me.L5C19.Size = New System.Drawing.Size(49, 50)
        Me.L5C19.TabIndex = 143
        Me.L5C19.TabStop = False
        '
        'L5C18
        '
        Me.L5C18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L5C18.Location = New System.Drawing.Point(842, 209)
        Me.L5C18.Margin = New System.Windows.Forms.Padding(0)
        Me.L5C18.Name = "L5C18"
        Me.L5C18.Size = New System.Drawing.Size(49, 50)
        Me.L5C18.TabIndex = 142
        Me.L5C18.TabStop = False
        '
        'L5C17
        '
        Me.L5C17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L5C17.Location = New System.Drawing.Point(793, 209)
        Me.L5C17.Margin = New System.Windows.Forms.Padding(0)
        Me.L5C17.Name = "L5C17"
        Me.L5C17.Size = New System.Drawing.Size(49, 50)
        Me.L5C17.TabIndex = 141
        Me.L5C17.TabStop = False
        '
        'L5C16
        '
        Me.L5C16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L5C16.Location = New System.Drawing.Point(744, 209)
        Me.L5C16.Margin = New System.Windows.Forms.Padding(0)
        Me.L5C16.Name = "L5C16"
        Me.L5C16.Size = New System.Drawing.Size(49, 50)
        Me.L5C16.TabIndex = 140
        Me.L5C16.TabStop = False
        '
        'L5C15
        '
        Me.L5C15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L5C15.Location = New System.Drawing.Point(695, 209)
        Me.L5C15.Margin = New System.Windows.Forms.Padding(0)
        Me.L5C15.Name = "L5C15"
        Me.L5C15.Size = New System.Drawing.Size(49, 50)
        Me.L5C15.TabIndex = 139
        Me.L5C15.TabStop = False
        '
        'L5C14
        '
        Me.L5C14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L5C14.Location = New System.Drawing.Point(646, 209)
        Me.L5C14.Margin = New System.Windows.Forms.Padding(0)
        Me.L5C14.Name = "L5C14"
        Me.L5C14.Size = New System.Drawing.Size(49, 50)
        Me.L5C14.TabIndex = 138
        Me.L5C14.TabStop = False
        '
        'L5C13
        '
        Me.L5C13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L5C13.Image = Global.interface_projet.My.Resources.Resources.CarreBleu
        Me.L5C13.Location = New System.Drawing.Point(597, 209)
        Me.L5C13.Margin = New System.Windows.Forms.Padding(0)
        Me.L5C13.Name = "L5C13"
        Me.L5C13.Size = New System.Drawing.Size(49, 50)
        Me.L5C13.TabIndex = 137
        Me.L5C13.TabStop = False
        '
        'L5C12
        '
        Me.L5C12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L5C12.Location = New System.Drawing.Point(548, 209)
        Me.L5C12.Margin = New System.Windows.Forms.Padding(0)
        Me.L5C12.Name = "L5C12"
        Me.L5C12.Size = New System.Drawing.Size(49, 50)
        Me.L5C12.TabIndex = 136
        Me.L5C12.TabStop = False
        '
        'L5C11
        '
        Me.L5C11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L5C11.Location = New System.Drawing.Point(499, 209)
        Me.L5C11.Margin = New System.Windows.Forms.Padding(0)
        Me.L5C11.Name = "L5C11"
        Me.L5C11.Size = New System.Drawing.Size(49, 50)
        Me.L5C11.TabIndex = 135
        Me.L5C11.TabStop = False
        '
        'L5C10
        '
        Me.L5C10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L5C10.Location = New System.Drawing.Point(450, 209)
        Me.L5C10.Margin = New System.Windows.Forms.Padding(0)
        Me.L5C10.Name = "L5C10"
        Me.L5C10.Size = New System.Drawing.Size(49, 50)
        Me.L5C10.TabIndex = 134
        Me.L5C10.TabStop = False
        '
        'L5C9
        '
        Me.L5C9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L5C9.Location = New System.Drawing.Point(401, 209)
        Me.L5C9.Margin = New System.Windows.Forms.Padding(0)
        Me.L5C9.Name = "L5C9"
        Me.L5C9.Size = New System.Drawing.Size(49, 50)
        Me.L5C9.TabIndex = 133
        Me.L5C9.TabStop = False
        '
        'L5C8
        '
        Me.L5C8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L5C8.Location = New System.Drawing.Point(352, 209)
        Me.L5C8.Margin = New System.Windows.Forms.Padding(0)
        Me.L5C8.Name = "L5C8"
        Me.L5C8.Size = New System.Drawing.Size(49, 50)
        Me.L5C8.TabIndex = 132
        Me.L5C8.TabStop = False
        '
        'L5C7
        '
        Me.L5C7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L5C7.Location = New System.Drawing.Point(303, 209)
        Me.L5C7.Margin = New System.Windows.Forms.Padding(0)
        Me.L5C7.Name = "L5C7"
        Me.L5C7.Size = New System.Drawing.Size(49, 50)
        Me.L5C7.TabIndex = 131
        Me.L5C7.TabStop = False
        '
        'L5C6
        '
        Me.L5C6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L5C6.Location = New System.Drawing.Point(254, 209)
        Me.L5C6.Margin = New System.Windows.Forms.Padding(0)
        Me.L5C6.Name = "L5C6"
        Me.L5C6.Size = New System.Drawing.Size(49, 50)
        Me.L5C6.TabIndex = 130
        Me.L5C6.TabStop = False
        '
        'L5C5
        '
        Me.L5C5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L5C5.Location = New System.Drawing.Point(205, 209)
        Me.L5C5.Margin = New System.Windows.Forms.Padding(0)
        Me.L5C5.Name = "L5C5"
        Me.L5C5.Size = New System.Drawing.Size(49, 50)
        Me.L5C5.TabIndex = 129
        Me.L5C5.TabStop = False
        '
        'L5C4
        '
        Me.L5C4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L5C4.Location = New System.Drawing.Point(156, 209)
        Me.L5C4.Margin = New System.Windows.Forms.Padding(0)
        Me.L5C4.Name = "L5C4"
        Me.L5C4.Size = New System.Drawing.Size(49, 50)
        Me.L5C4.TabIndex = 128
        Me.L5C4.TabStop = False
        '
        'L5C3
        '
        Me.L5C3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L5C3.Location = New System.Drawing.Point(107, 209)
        Me.L5C3.Margin = New System.Windows.Forms.Padding(0)
        Me.L5C3.Name = "L5C3"
        Me.L5C3.Size = New System.Drawing.Size(49, 50)
        Me.L5C3.TabIndex = 127
        Me.L5C3.TabStop = False
        '
        'L5C2
        '
        Me.L5C2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L5C2.Location = New System.Drawing.Point(58, 209)
        Me.L5C2.Margin = New System.Windows.Forms.Padding(0)
        Me.L5C2.Name = "L5C2"
        Me.L5C2.Size = New System.Drawing.Size(49, 50)
        Me.L5C2.TabIndex = 126
        Me.L5C2.TabStop = False
        '
        'L5C1
        '
        Me.L5C1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L5C1.Location = New System.Drawing.Point(9, 209)
        Me.L5C1.Margin = New System.Windows.Forms.Padding(0)
        Me.L5C1.Name = "L5C1"
        Me.L5C1.Size = New System.Drawing.Size(49, 50)
        Me.L5C1.TabIndex = 125
        Me.L5C1.TabStop = False
        '
        'L6C20
        '
        Me.L6C20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L6C20.Location = New System.Drawing.Point(940, 259)
        Me.L6C20.Margin = New System.Windows.Forms.Padding(0)
        Me.L6C20.Name = "L6C20"
        Me.L6C20.Size = New System.Drawing.Size(49, 50)
        Me.L6C20.TabIndex = 164
        Me.L6C20.TabStop = False
        '
        'L6C19
        '
        Me.L6C19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L6C19.Location = New System.Drawing.Point(891, 259)
        Me.L6C19.Margin = New System.Windows.Forms.Padding(0)
        Me.L6C19.Name = "L6C19"
        Me.L6C19.Size = New System.Drawing.Size(49, 50)
        Me.L6C19.TabIndex = 163
        Me.L6C19.TabStop = False
        '
        'L6C18
        '
        Me.L6C18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L6C18.Location = New System.Drawing.Point(842, 259)
        Me.L6C18.Margin = New System.Windows.Forms.Padding(0)
        Me.L6C18.Name = "L6C18"
        Me.L6C18.Size = New System.Drawing.Size(49, 50)
        Me.L6C18.TabIndex = 162
        Me.L6C18.TabStop = False
        '
        'L6C17
        '
        Me.L6C17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L6C17.Location = New System.Drawing.Point(793, 259)
        Me.L6C17.Margin = New System.Windows.Forms.Padding(0)
        Me.L6C17.Name = "L6C17"
        Me.L6C17.Size = New System.Drawing.Size(49, 50)
        Me.L6C17.TabIndex = 161
        Me.L6C17.TabStop = False
        '
        'L6C16
        '
        Me.L6C16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L6C16.Location = New System.Drawing.Point(744, 259)
        Me.L6C16.Margin = New System.Windows.Forms.Padding(0)
        Me.L6C16.Name = "L6C16"
        Me.L6C16.Size = New System.Drawing.Size(49, 50)
        Me.L6C16.TabIndex = 160
        Me.L6C16.TabStop = False
        '
        'L6C15
        '
        Me.L6C15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L6C15.Location = New System.Drawing.Point(695, 259)
        Me.L6C15.Margin = New System.Windows.Forms.Padding(0)
        Me.L6C15.Name = "L6C15"
        Me.L6C15.Size = New System.Drawing.Size(49, 50)
        Me.L6C15.TabIndex = 159
        Me.L6C15.TabStop = False
        '
        'L6C14
        '
        Me.L6C14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L6C14.Location = New System.Drawing.Point(646, 259)
        Me.L6C14.Margin = New System.Windows.Forms.Padding(0)
        Me.L6C14.Name = "L6C14"
        Me.L6C14.Size = New System.Drawing.Size(49, 50)
        Me.L6C14.TabIndex = 158
        Me.L6C14.TabStop = False
        '
        'L6C13
        '
        Me.L6C13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L6C13.Location = New System.Drawing.Point(597, 259)
        Me.L6C13.Margin = New System.Windows.Forms.Padding(0)
        Me.L6C13.Name = "L6C13"
        Me.L6C13.Size = New System.Drawing.Size(49, 50)
        Me.L6C13.TabIndex = 157
        Me.L6C13.TabStop = False
        '
        'L6C12
        '
        Me.L6C12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L6C12.Location = New System.Drawing.Point(548, 259)
        Me.L6C12.Margin = New System.Windows.Forms.Padding(0)
        Me.L6C12.Name = "L6C12"
        Me.L6C12.Size = New System.Drawing.Size(49, 50)
        Me.L6C12.TabIndex = 156
        Me.L6C12.TabStop = False
        '
        'L6C11
        '
        Me.L6C11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L6C11.Location = New System.Drawing.Point(499, 259)
        Me.L6C11.Margin = New System.Windows.Forms.Padding(0)
        Me.L6C11.Name = "L6C11"
        Me.L6C11.Size = New System.Drawing.Size(49, 50)
        Me.L6C11.TabIndex = 155
        Me.L6C11.TabStop = False
        '
        'L6C10
        '
        Me.L6C10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L6C10.Location = New System.Drawing.Point(450, 259)
        Me.L6C10.Margin = New System.Windows.Forms.Padding(0)
        Me.L6C10.Name = "L6C10"
        Me.L6C10.Size = New System.Drawing.Size(49, 50)
        Me.L6C10.TabIndex = 154
        Me.L6C10.TabStop = False
        '
        'L6C9
        '
        Me.L6C9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L6C9.Location = New System.Drawing.Point(401, 259)
        Me.L6C9.Margin = New System.Windows.Forms.Padding(0)
        Me.L6C9.Name = "L6C9"
        Me.L6C9.Size = New System.Drawing.Size(49, 50)
        Me.L6C9.TabIndex = 153
        Me.L6C9.TabStop = False
        '
        'L6C8
        '
        Me.L6C8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L6C8.Location = New System.Drawing.Point(352, 259)
        Me.L6C8.Margin = New System.Windows.Forms.Padding(0)
        Me.L6C8.Name = "L6C8"
        Me.L6C8.Size = New System.Drawing.Size(49, 50)
        Me.L6C8.TabIndex = 152
        Me.L6C8.TabStop = False
        '
        'L6C7
        '
        Me.L6C7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L6C7.Location = New System.Drawing.Point(303, 259)
        Me.L6C7.Margin = New System.Windows.Forms.Padding(0)
        Me.L6C7.Name = "L6C7"
        Me.L6C7.Size = New System.Drawing.Size(49, 50)
        Me.L6C7.TabIndex = 151
        Me.L6C7.TabStop = False
        '
        'L6C6
        '
        Me.L6C6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L6C6.Location = New System.Drawing.Point(254, 259)
        Me.L6C6.Margin = New System.Windows.Forms.Padding(0)
        Me.L6C6.Name = "L6C6"
        Me.L6C6.Size = New System.Drawing.Size(49, 50)
        Me.L6C6.TabIndex = 150
        Me.L6C6.TabStop = False
        '
        'L6C5
        '
        Me.L6C5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L6C5.Location = New System.Drawing.Point(205, 259)
        Me.L6C5.Margin = New System.Windows.Forms.Padding(0)
        Me.L6C5.Name = "L6C5"
        Me.L6C5.Size = New System.Drawing.Size(49, 50)
        Me.L6C5.TabIndex = 149
        Me.L6C5.TabStop = False
        '
        'L6C4
        '
        Me.L6C4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L6C4.Location = New System.Drawing.Point(156, 259)
        Me.L6C4.Margin = New System.Windows.Forms.Padding(0)
        Me.L6C4.Name = "L6C4"
        Me.L6C4.Size = New System.Drawing.Size(49, 50)
        Me.L6C4.TabIndex = 148
        Me.L6C4.TabStop = False
        '
        'L6C3
        '
        Me.L6C3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L6C3.Location = New System.Drawing.Point(107, 259)
        Me.L6C3.Margin = New System.Windows.Forms.Padding(0)
        Me.L6C3.Name = "L6C3"
        Me.L6C3.Size = New System.Drawing.Size(49, 50)
        Me.L6C3.TabIndex = 147
        Me.L6C3.TabStop = False
        '
        'L6C2
        '
        Me.L6C2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L6C2.Location = New System.Drawing.Point(58, 259)
        Me.L6C2.Margin = New System.Windows.Forms.Padding(0)
        Me.L6C2.Name = "L6C2"
        Me.L6C2.Size = New System.Drawing.Size(49, 50)
        Me.L6C2.TabIndex = 146
        Me.L6C2.TabStop = False
        '
        'L6C1
        '
        Me.L6C1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L6C1.Location = New System.Drawing.Point(9, 259)
        Me.L6C1.Margin = New System.Windows.Forms.Padding(0)
        Me.L6C1.Name = "L6C1"
        Me.L6C1.Size = New System.Drawing.Size(49, 50)
        Me.L6C1.TabIndex = 145
        Me.L6C1.TabStop = False
        '
        'L7C20
        '
        Me.L7C20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L7C20.Location = New System.Drawing.Point(940, 309)
        Me.L7C20.Margin = New System.Windows.Forms.Padding(0)
        Me.L7C20.Name = "L7C20"
        Me.L7C20.Size = New System.Drawing.Size(49, 50)
        Me.L7C20.TabIndex = 184
        Me.L7C20.TabStop = False
        '
        'L7C19
        '
        Me.L7C19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L7C19.Location = New System.Drawing.Point(891, 309)
        Me.L7C19.Margin = New System.Windows.Forms.Padding(0)
        Me.L7C19.Name = "L7C19"
        Me.L7C19.Size = New System.Drawing.Size(49, 50)
        Me.L7C19.TabIndex = 183
        Me.L7C19.TabStop = False
        '
        'L7C18
        '
        Me.L7C18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L7C18.Location = New System.Drawing.Point(842, 309)
        Me.L7C18.Margin = New System.Windows.Forms.Padding(0)
        Me.L7C18.Name = "L7C18"
        Me.L7C18.Size = New System.Drawing.Size(49, 50)
        Me.L7C18.TabIndex = 182
        Me.L7C18.TabStop = False
        '
        'L7C17
        '
        Me.L7C17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L7C17.Location = New System.Drawing.Point(793, 309)
        Me.L7C17.Margin = New System.Windows.Forms.Padding(0)
        Me.L7C17.Name = "L7C17"
        Me.L7C17.Size = New System.Drawing.Size(49, 50)
        Me.L7C17.TabIndex = 181
        Me.L7C17.TabStop = False
        '
        'L7C16
        '
        Me.L7C16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L7C16.Location = New System.Drawing.Point(744, 309)
        Me.L7C16.Margin = New System.Windows.Forms.Padding(0)
        Me.L7C16.Name = "L7C16"
        Me.L7C16.Size = New System.Drawing.Size(49, 50)
        Me.L7C16.TabIndex = 180
        Me.L7C16.TabStop = False
        '
        'L7C15
        '
        Me.L7C15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L7C15.Location = New System.Drawing.Point(695, 309)
        Me.L7C15.Margin = New System.Windows.Forms.Padding(0)
        Me.L7C15.Name = "L7C15"
        Me.L7C15.Size = New System.Drawing.Size(49, 50)
        Me.L7C15.TabIndex = 179
        Me.L7C15.TabStop = False
        '
        'L7C14
        '
        Me.L7C14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L7C14.Location = New System.Drawing.Point(646, 309)
        Me.L7C14.Margin = New System.Windows.Forms.Padding(0)
        Me.L7C14.Name = "L7C14"
        Me.L7C14.Size = New System.Drawing.Size(49, 50)
        Me.L7C14.TabIndex = 178
        Me.L7C14.TabStop = False
        '
        'L7C13
        '
        Me.L7C13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L7C13.Location = New System.Drawing.Point(597, 309)
        Me.L7C13.Margin = New System.Windows.Forms.Padding(0)
        Me.L7C13.Name = "L7C13"
        Me.L7C13.Size = New System.Drawing.Size(49, 50)
        Me.L7C13.TabIndex = 177
        Me.L7C13.TabStop = False
        '
        'L7C12
        '
        Me.L7C12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L7C12.Location = New System.Drawing.Point(548, 309)
        Me.L7C12.Margin = New System.Windows.Forms.Padding(0)
        Me.L7C12.Name = "L7C12"
        Me.L7C12.Size = New System.Drawing.Size(49, 50)
        Me.L7C12.TabIndex = 176
        Me.L7C12.TabStop = False
        '
        'L7C11
        '
        Me.L7C11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L7C11.Location = New System.Drawing.Point(499, 309)
        Me.L7C11.Margin = New System.Windows.Forms.Padding(0)
        Me.L7C11.Name = "L7C11"
        Me.L7C11.Size = New System.Drawing.Size(49, 50)
        Me.L7C11.TabIndex = 175
        Me.L7C11.TabStop = False
        '
        'L7C10
        '
        Me.L7C10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L7C10.Location = New System.Drawing.Point(450, 309)
        Me.L7C10.Margin = New System.Windows.Forms.Padding(0)
        Me.L7C10.Name = "L7C10"
        Me.L7C10.Size = New System.Drawing.Size(49, 50)
        Me.L7C10.TabIndex = 174
        Me.L7C10.TabStop = False
        '
        'L7C9
        '
        Me.L7C9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L7C9.Location = New System.Drawing.Point(401, 309)
        Me.L7C9.Margin = New System.Windows.Forms.Padding(0)
        Me.L7C9.Name = "L7C9"
        Me.L7C9.Size = New System.Drawing.Size(49, 50)
        Me.L7C9.TabIndex = 173
        Me.L7C9.TabStop = False
        '
        'L7C8
        '
        Me.L7C8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L7C8.Location = New System.Drawing.Point(352, 309)
        Me.L7C8.Margin = New System.Windows.Forms.Padding(0)
        Me.L7C8.Name = "L7C8"
        Me.L7C8.Size = New System.Drawing.Size(49, 50)
        Me.L7C8.TabIndex = 172
        Me.L7C8.TabStop = False
        '
        'L7C7
        '
        Me.L7C7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L7C7.Location = New System.Drawing.Point(303, 309)
        Me.L7C7.Margin = New System.Windows.Forms.Padding(0)
        Me.L7C7.Name = "L7C7"
        Me.L7C7.Size = New System.Drawing.Size(49, 50)
        Me.L7C7.TabIndex = 171
        Me.L7C7.TabStop = False
        '
        'L7C6
        '
        Me.L7C6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L7C6.Location = New System.Drawing.Point(254, 309)
        Me.L7C6.Margin = New System.Windows.Forms.Padding(0)
        Me.L7C6.Name = "L7C6"
        Me.L7C6.Size = New System.Drawing.Size(49, 50)
        Me.L7C6.TabIndex = 170
        Me.L7C6.TabStop = False
        '
        'L7C5
        '
        Me.L7C5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L7C5.Location = New System.Drawing.Point(205, 309)
        Me.L7C5.Margin = New System.Windows.Forms.Padding(0)
        Me.L7C5.Name = "L7C5"
        Me.L7C5.Size = New System.Drawing.Size(49, 50)
        Me.L7C5.TabIndex = 169
        Me.L7C5.TabStop = False
        '
        'L7C4
        '
        Me.L7C4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L7C4.Location = New System.Drawing.Point(156, 309)
        Me.L7C4.Margin = New System.Windows.Forms.Padding(0)
        Me.L7C4.Name = "L7C4"
        Me.L7C4.Size = New System.Drawing.Size(49, 50)
        Me.L7C4.TabIndex = 168
        Me.L7C4.TabStop = False
        '
        'L7C3
        '
        Me.L7C3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L7C3.Location = New System.Drawing.Point(107, 309)
        Me.L7C3.Margin = New System.Windows.Forms.Padding(0)
        Me.L7C3.Name = "L7C3"
        Me.L7C3.Size = New System.Drawing.Size(49, 50)
        Me.L7C3.TabIndex = 167
        Me.L7C3.TabStop = False
        '
        'L7C2
        '
        Me.L7C2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L7C2.Location = New System.Drawing.Point(58, 309)
        Me.L7C2.Margin = New System.Windows.Forms.Padding(0)
        Me.L7C2.Name = "L7C2"
        Me.L7C2.Size = New System.Drawing.Size(49, 50)
        Me.L7C2.TabIndex = 166
        Me.L7C2.TabStop = False
        '
        'L7C1
        '
        Me.L7C1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L7C1.Location = New System.Drawing.Point(9, 309)
        Me.L7C1.Margin = New System.Windows.Forms.Padding(0)
        Me.L7C1.Name = "L7C1"
        Me.L7C1.Size = New System.Drawing.Size(49, 50)
        Me.L7C1.TabIndex = 165
        Me.L7C1.TabStop = False
        '
        'L9C20
        '
        Me.L9C20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L9C20.Location = New System.Drawing.Point(940, 409)
        Me.L9C20.Margin = New System.Windows.Forms.Padding(0)
        Me.L9C20.Name = "L9C20"
        Me.L9C20.Size = New System.Drawing.Size(49, 50)
        Me.L9C20.TabIndex = 224
        Me.L9C20.TabStop = False
        '
        'L9C19
        '
        Me.L9C19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L9C19.Location = New System.Drawing.Point(891, 409)
        Me.L9C19.Margin = New System.Windows.Forms.Padding(0)
        Me.L9C19.Name = "L9C19"
        Me.L9C19.Size = New System.Drawing.Size(49, 50)
        Me.L9C19.TabIndex = 223
        Me.L9C19.TabStop = False
        '
        'L9C18
        '
        Me.L9C18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L9C18.Location = New System.Drawing.Point(842, 409)
        Me.L9C18.Margin = New System.Windows.Forms.Padding(0)
        Me.L9C18.Name = "L9C18"
        Me.L9C18.Size = New System.Drawing.Size(49, 50)
        Me.L9C18.TabIndex = 222
        Me.L9C18.TabStop = False
        '
        'L9C17
        '
        Me.L9C17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L9C17.Location = New System.Drawing.Point(793, 409)
        Me.L9C17.Margin = New System.Windows.Forms.Padding(0)
        Me.L9C17.Name = "L9C17"
        Me.L9C17.Size = New System.Drawing.Size(49, 50)
        Me.L9C17.TabIndex = 221
        Me.L9C17.TabStop = False
        '
        'L9C16
        '
        Me.L9C16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L9C16.Location = New System.Drawing.Point(744, 409)
        Me.L9C16.Margin = New System.Windows.Forms.Padding(0)
        Me.L9C16.Name = "L9C16"
        Me.L9C16.Size = New System.Drawing.Size(49, 50)
        Me.L9C16.TabIndex = 220
        Me.L9C16.TabStop = False
        '
        'L9C15
        '
        Me.L9C15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L9C15.Location = New System.Drawing.Point(695, 409)
        Me.L9C15.Margin = New System.Windows.Forms.Padding(0)
        Me.L9C15.Name = "L9C15"
        Me.L9C15.Size = New System.Drawing.Size(49, 50)
        Me.L9C15.TabIndex = 219
        Me.L9C15.TabStop = False
        '
        'L9C14
        '
        Me.L9C14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L9C14.Location = New System.Drawing.Point(646, 409)
        Me.L9C14.Margin = New System.Windows.Forms.Padding(0)
        Me.L9C14.Name = "L9C14"
        Me.L9C14.Size = New System.Drawing.Size(49, 50)
        Me.L9C14.TabIndex = 218
        Me.L9C14.TabStop = False
        '
        'L9C13
        '
        Me.L9C13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L9C13.Location = New System.Drawing.Point(597, 409)
        Me.L9C13.Margin = New System.Windows.Forms.Padding(0)
        Me.L9C13.Name = "L9C13"
        Me.L9C13.Size = New System.Drawing.Size(49, 50)
        Me.L9C13.TabIndex = 217
        Me.L9C13.TabStop = False
        '
        'L9C12
        '
        Me.L9C12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L9C12.Location = New System.Drawing.Point(548, 409)
        Me.L9C12.Margin = New System.Windows.Forms.Padding(0)
        Me.L9C12.Name = "L9C12"
        Me.L9C12.Size = New System.Drawing.Size(49, 50)
        Me.L9C12.TabIndex = 216
        Me.L9C12.TabStop = False
        '
        'L9C11
        '
        Me.L9C11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L9C11.Location = New System.Drawing.Point(499, 409)
        Me.L9C11.Margin = New System.Windows.Forms.Padding(0)
        Me.L9C11.Name = "L9C11"
        Me.L9C11.Size = New System.Drawing.Size(49, 50)
        Me.L9C11.TabIndex = 215
        Me.L9C11.TabStop = False
        '
        'L9C10
        '
        Me.L9C10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L9C10.Location = New System.Drawing.Point(450, 409)
        Me.L9C10.Margin = New System.Windows.Forms.Padding(0)
        Me.L9C10.Name = "L9C10"
        Me.L9C10.Size = New System.Drawing.Size(49, 50)
        Me.L9C10.TabIndex = 214
        Me.L9C10.TabStop = False
        '
        'L9C9
        '
        Me.L9C9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L9C9.Location = New System.Drawing.Point(401, 409)
        Me.L9C9.Margin = New System.Windows.Forms.Padding(0)
        Me.L9C9.Name = "L9C9"
        Me.L9C9.Size = New System.Drawing.Size(49, 50)
        Me.L9C9.TabIndex = 213
        Me.L9C9.TabStop = False
        '
        'L9C8
        '
        Me.L9C8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L9C8.Location = New System.Drawing.Point(352, 409)
        Me.L9C8.Margin = New System.Windows.Forms.Padding(0)
        Me.L9C8.Name = "L9C8"
        Me.L9C8.Size = New System.Drawing.Size(49, 50)
        Me.L9C8.TabIndex = 212
        Me.L9C8.TabStop = False
        '
        'L9C7
        '
        Me.L9C7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L9C7.Location = New System.Drawing.Point(303, 409)
        Me.L9C7.Margin = New System.Windows.Forms.Padding(0)
        Me.L9C7.Name = "L9C7"
        Me.L9C7.Size = New System.Drawing.Size(49, 50)
        Me.L9C7.TabIndex = 211
        Me.L9C7.TabStop = False
        '
        'L9C6
        '
        Me.L9C6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L9C6.Location = New System.Drawing.Point(254, 409)
        Me.L9C6.Margin = New System.Windows.Forms.Padding(0)
        Me.L9C6.Name = "L9C6"
        Me.L9C6.Size = New System.Drawing.Size(49, 50)
        Me.L9C6.TabIndex = 210
        Me.L9C6.TabStop = False
        '
        'L9C5
        '
        Me.L9C5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L9C5.Location = New System.Drawing.Point(205, 409)
        Me.L9C5.Margin = New System.Windows.Forms.Padding(0)
        Me.L9C5.Name = "L9C5"
        Me.L9C5.Size = New System.Drawing.Size(49, 50)
        Me.L9C5.TabIndex = 209
        Me.L9C5.TabStop = False
        '
        'L9C4
        '
        Me.L9C4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L9C4.Location = New System.Drawing.Point(156, 409)
        Me.L9C4.Margin = New System.Windows.Forms.Padding(0)
        Me.L9C4.Name = "L9C4"
        Me.L9C4.Size = New System.Drawing.Size(49, 50)
        Me.L9C4.TabIndex = 208
        Me.L9C4.TabStop = False
        '
        'L9C3
        '
        Me.L9C3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L9C3.Location = New System.Drawing.Point(107, 409)
        Me.L9C3.Margin = New System.Windows.Forms.Padding(0)
        Me.L9C3.Name = "L9C3"
        Me.L9C3.Size = New System.Drawing.Size(49, 50)
        Me.L9C3.TabIndex = 207
        Me.L9C3.TabStop = False
        '
        'L9C2
        '
        Me.L9C2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L9C2.Location = New System.Drawing.Point(58, 409)
        Me.L9C2.Margin = New System.Windows.Forms.Padding(0)
        Me.L9C2.Name = "L9C2"
        Me.L9C2.Size = New System.Drawing.Size(49, 50)
        Me.L9C2.TabIndex = 206
        Me.L9C2.TabStop = False
        '
        'L9C1
        '
        Me.L9C1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L9C1.Location = New System.Drawing.Point(9, 409)
        Me.L9C1.Margin = New System.Windows.Forms.Padding(0)
        Me.L9C1.Name = "L9C1"
        Me.L9C1.Size = New System.Drawing.Size(49, 50)
        Me.L9C1.TabIndex = 205
        Me.L9C1.TabStop = False
        '
        'L8C20
        '
        Me.L8C20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L8C20.Location = New System.Drawing.Point(940, 359)
        Me.L8C20.Margin = New System.Windows.Forms.Padding(0)
        Me.L8C20.Name = "L8C20"
        Me.L8C20.Size = New System.Drawing.Size(49, 50)
        Me.L8C20.TabIndex = 204
        Me.L8C20.TabStop = False
        '
        'L8C19
        '
        Me.L8C19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L8C19.Location = New System.Drawing.Point(891, 359)
        Me.L8C19.Margin = New System.Windows.Forms.Padding(0)
        Me.L8C19.Name = "L8C19"
        Me.L8C19.Size = New System.Drawing.Size(49, 50)
        Me.L8C19.TabIndex = 203
        Me.L8C19.TabStop = False
        '
        'L8C18
        '
        Me.L8C18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L8C18.Location = New System.Drawing.Point(842, 359)
        Me.L8C18.Margin = New System.Windows.Forms.Padding(0)
        Me.L8C18.Name = "L8C18"
        Me.L8C18.Size = New System.Drawing.Size(49, 50)
        Me.L8C18.TabIndex = 202
        Me.L8C18.TabStop = False
        '
        'L8C17
        '
        Me.L8C17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L8C17.Location = New System.Drawing.Point(793, 359)
        Me.L8C17.Margin = New System.Windows.Forms.Padding(0)
        Me.L8C17.Name = "L8C17"
        Me.L8C17.Size = New System.Drawing.Size(49, 50)
        Me.L8C17.TabIndex = 201
        Me.L8C17.TabStop = False
        '
        'L8C16
        '
        Me.L8C16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L8C16.Location = New System.Drawing.Point(744, 359)
        Me.L8C16.Margin = New System.Windows.Forms.Padding(0)
        Me.L8C16.Name = "L8C16"
        Me.L8C16.Size = New System.Drawing.Size(49, 50)
        Me.L8C16.TabIndex = 200
        Me.L8C16.TabStop = False
        '
        'L8C15
        '
        Me.L8C15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L8C15.Location = New System.Drawing.Point(695, 359)
        Me.L8C15.Margin = New System.Windows.Forms.Padding(0)
        Me.L8C15.Name = "L8C15"
        Me.L8C15.Size = New System.Drawing.Size(49, 50)
        Me.L8C15.TabIndex = 199
        Me.L8C15.TabStop = False
        '
        'L8C14
        '
        Me.L8C14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L8C14.Location = New System.Drawing.Point(646, 359)
        Me.L8C14.Margin = New System.Windows.Forms.Padding(0)
        Me.L8C14.Name = "L8C14"
        Me.L8C14.Size = New System.Drawing.Size(49, 50)
        Me.L8C14.TabIndex = 198
        Me.L8C14.TabStop = False
        '
        'L8C13
        '
        Me.L8C13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L8C13.Location = New System.Drawing.Point(597, 359)
        Me.L8C13.Margin = New System.Windows.Forms.Padding(0)
        Me.L8C13.Name = "L8C13"
        Me.L8C13.Size = New System.Drawing.Size(49, 50)
        Me.L8C13.TabIndex = 197
        Me.L8C13.TabStop = False
        '
        'L8C12
        '
        Me.L8C12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L8C12.Location = New System.Drawing.Point(548, 359)
        Me.L8C12.Margin = New System.Windows.Forms.Padding(0)
        Me.L8C12.Name = "L8C12"
        Me.L8C12.Size = New System.Drawing.Size(49, 50)
        Me.L8C12.TabIndex = 196
        Me.L8C12.TabStop = False
        '
        'L8C11
        '
        Me.L8C11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L8C11.Location = New System.Drawing.Point(499, 359)
        Me.L8C11.Margin = New System.Windows.Forms.Padding(0)
        Me.L8C11.Name = "L8C11"
        Me.L8C11.Size = New System.Drawing.Size(49, 50)
        Me.L8C11.TabIndex = 195
        Me.L8C11.TabStop = False
        '
        'L8C10
        '
        Me.L8C10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L8C10.Location = New System.Drawing.Point(450, 359)
        Me.L8C10.Margin = New System.Windows.Forms.Padding(0)
        Me.L8C10.Name = "L8C10"
        Me.L8C10.Size = New System.Drawing.Size(49, 50)
        Me.L8C10.TabIndex = 194
        Me.L8C10.TabStop = False
        '
        'L8C9
        '
        Me.L8C9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L8C9.Location = New System.Drawing.Point(401, 359)
        Me.L8C9.Margin = New System.Windows.Forms.Padding(0)
        Me.L8C9.Name = "L8C9"
        Me.L8C9.Size = New System.Drawing.Size(49, 50)
        Me.L8C9.TabIndex = 193
        Me.L8C9.TabStop = False
        '
        'L8C8
        '
        Me.L8C8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L8C8.Location = New System.Drawing.Point(352, 359)
        Me.L8C8.Margin = New System.Windows.Forms.Padding(0)
        Me.L8C8.Name = "L8C8"
        Me.L8C8.Size = New System.Drawing.Size(49, 50)
        Me.L8C8.TabIndex = 192
        Me.L8C8.TabStop = False
        '
        'L8C7
        '
        Me.L8C7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L8C7.Location = New System.Drawing.Point(303, 359)
        Me.L8C7.Margin = New System.Windows.Forms.Padding(0)
        Me.L8C7.Name = "L8C7"
        Me.L8C7.Size = New System.Drawing.Size(49, 50)
        Me.L8C7.TabIndex = 191
        Me.L8C7.TabStop = False
        '
        'L8C6
        '
        Me.L8C6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L8C6.Location = New System.Drawing.Point(254, 359)
        Me.L8C6.Margin = New System.Windows.Forms.Padding(0)
        Me.L8C6.Name = "L8C6"
        Me.L8C6.Size = New System.Drawing.Size(49, 50)
        Me.L8C6.TabIndex = 190
        Me.L8C6.TabStop = False
        '
        'L8C5
        '
        Me.L8C5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L8C5.Location = New System.Drawing.Point(205, 359)
        Me.L8C5.Margin = New System.Windows.Forms.Padding(0)
        Me.L8C5.Name = "L8C5"
        Me.L8C5.Size = New System.Drawing.Size(49, 50)
        Me.L8C5.TabIndex = 189
        Me.L8C5.TabStop = False
        '
        'L8C4
        '
        Me.L8C4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L8C4.Location = New System.Drawing.Point(156, 359)
        Me.L8C4.Margin = New System.Windows.Forms.Padding(0)
        Me.L8C4.Name = "L8C4"
        Me.L8C4.Size = New System.Drawing.Size(49, 50)
        Me.L8C4.TabIndex = 188
        Me.L8C4.TabStop = False
        '
        'L8C3
        '
        Me.L8C3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L8C3.Location = New System.Drawing.Point(107, 359)
        Me.L8C3.Margin = New System.Windows.Forms.Padding(0)
        Me.L8C3.Name = "L8C3"
        Me.L8C3.Size = New System.Drawing.Size(49, 50)
        Me.L8C3.TabIndex = 187
        Me.L8C3.TabStop = False
        '
        'L8C2
        '
        Me.L8C2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L8C2.Location = New System.Drawing.Point(58, 359)
        Me.L8C2.Margin = New System.Windows.Forms.Padding(0)
        Me.L8C2.Name = "L8C2"
        Me.L8C2.Size = New System.Drawing.Size(49, 50)
        Me.L8C2.TabIndex = 186
        Me.L8C2.TabStop = False
        '
        'L8C1
        '
        Me.L8C1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L8C1.Location = New System.Drawing.Point(9, 359)
        Me.L8C1.Margin = New System.Windows.Forms.Padding(0)
        Me.L8C1.Name = "L8C1"
        Me.L8C1.Size = New System.Drawing.Size(49, 50)
        Me.L8C1.TabIndex = 185
        Me.L8C1.TabStop = False
        '
        'PictureBox1
        '
        Me.PictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox1.Location = New System.Drawing.Point(940, 459)
        Me.PictureBox1.Margin = New System.Windows.Forms.Padding(0)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(49, 50)
        Me.PictureBox1.TabIndex = 244
        Me.PictureBox1.TabStop = False
        '
        'L10C19
        '
        Me.L10C19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L10C19.Location = New System.Drawing.Point(891, 459)
        Me.L10C19.Margin = New System.Windows.Forms.Padding(0)
        Me.L10C19.Name = "L10C19"
        Me.L10C19.Size = New System.Drawing.Size(49, 50)
        Me.L10C19.TabIndex = 243
        Me.L10C19.TabStop = False
        '
        'L10C18
        '
        Me.L10C18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L10C18.Location = New System.Drawing.Point(842, 459)
        Me.L10C18.Margin = New System.Windows.Forms.Padding(0)
        Me.L10C18.Name = "L10C18"
        Me.L10C18.Size = New System.Drawing.Size(49, 50)
        Me.L10C18.TabIndex = 242
        Me.L10C18.TabStop = False
        '
        'L10C17
        '
        Me.L10C17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L10C17.Location = New System.Drawing.Point(793, 459)
        Me.L10C17.Margin = New System.Windows.Forms.Padding(0)
        Me.L10C17.Name = "L10C17"
        Me.L10C17.Size = New System.Drawing.Size(49, 50)
        Me.L10C17.TabIndex = 241
        Me.L10C17.TabStop = False
        '
        'L10C16
        '
        Me.L10C16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L10C16.Location = New System.Drawing.Point(744, 459)
        Me.L10C16.Margin = New System.Windows.Forms.Padding(0)
        Me.L10C16.Name = "L10C16"
        Me.L10C16.Size = New System.Drawing.Size(49, 50)
        Me.L10C16.TabIndex = 240
        Me.L10C16.TabStop = False
        '
        'L10C15
        '
        Me.L10C15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L10C15.Location = New System.Drawing.Point(695, 459)
        Me.L10C15.Margin = New System.Windows.Forms.Padding(0)
        Me.L10C15.Name = "L10C15"
        Me.L10C15.Size = New System.Drawing.Size(49, 50)
        Me.L10C15.TabIndex = 239
        Me.L10C15.TabStop = False
        '
        'L10C14
        '
        Me.L10C14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L10C14.Location = New System.Drawing.Point(646, 459)
        Me.L10C14.Margin = New System.Windows.Forms.Padding(0)
        Me.L10C14.Name = "L10C14"
        Me.L10C14.Size = New System.Drawing.Size(49, 50)
        Me.L10C14.TabIndex = 238
        Me.L10C14.TabStop = False
        '
        'L10C13
        '
        Me.L10C13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L10C13.Location = New System.Drawing.Point(597, 459)
        Me.L10C13.Margin = New System.Windows.Forms.Padding(0)
        Me.L10C13.Name = "L10C13"
        Me.L10C13.Size = New System.Drawing.Size(49, 50)
        Me.L10C13.TabIndex = 237
        Me.L10C13.TabStop = False
        '
        'L10C12
        '
        Me.L10C12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L10C12.Location = New System.Drawing.Point(548, 459)
        Me.L10C12.Margin = New System.Windows.Forms.Padding(0)
        Me.L10C12.Name = "L10C12"
        Me.L10C12.Size = New System.Drawing.Size(49, 50)
        Me.L10C12.TabIndex = 236
        Me.L10C12.TabStop = False
        '
        'L10C11
        '
        Me.L10C11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L10C11.Location = New System.Drawing.Point(499, 459)
        Me.L10C11.Margin = New System.Windows.Forms.Padding(0)
        Me.L10C11.Name = "L10C11"
        Me.L10C11.Size = New System.Drawing.Size(49, 50)
        Me.L10C11.TabIndex = 235
        Me.L10C11.TabStop = False
        '
        'L10C10
        '
        Me.L10C10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L10C10.Location = New System.Drawing.Point(450, 459)
        Me.L10C10.Margin = New System.Windows.Forms.Padding(0)
        Me.L10C10.Name = "L10C10"
        Me.L10C10.Size = New System.Drawing.Size(49, 50)
        Me.L10C10.TabIndex = 234
        Me.L10C10.TabStop = False
        '
        'L10C9
        '
        Me.L10C9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L10C9.Location = New System.Drawing.Point(401, 459)
        Me.L10C9.Margin = New System.Windows.Forms.Padding(0)
        Me.L10C9.Name = "L10C9"
        Me.L10C9.Size = New System.Drawing.Size(49, 50)
        Me.L10C9.TabIndex = 233
        Me.L10C9.TabStop = False
        '
        'L10C8
        '
        Me.L10C8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L10C8.Location = New System.Drawing.Point(352, 459)
        Me.L10C8.Margin = New System.Windows.Forms.Padding(0)
        Me.L10C8.Name = "L10C8"
        Me.L10C8.Size = New System.Drawing.Size(49, 50)
        Me.L10C8.TabIndex = 232
        Me.L10C8.TabStop = False
        '
        'L10C7
        '
        Me.L10C7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L10C7.Location = New System.Drawing.Point(303, 459)
        Me.L10C7.Margin = New System.Windows.Forms.Padding(0)
        Me.L10C7.Name = "L10C7"
        Me.L10C7.Size = New System.Drawing.Size(49, 50)
        Me.L10C7.TabIndex = 231
        Me.L10C7.TabStop = False
        '
        'L10C6
        '
        Me.L10C6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L10C6.Location = New System.Drawing.Point(254, 459)
        Me.L10C6.Margin = New System.Windows.Forms.Padding(0)
        Me.L10C6.Name = "L10C6"
        Me.L10C6.Size = New System.Drawing.Size(49, 50)
        Me.L10C6.TabIndex = 230
        Me.L10C6.TabStop = False
        '
        'L10C5
        '
        Me.L10C5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L10C5.Location = New System.Drawing.Point(205, 459)
        Me.L10C5.Margin = New System.Windows.Forms.Padding(0)
        Me.L10C5.Name = "L10C5"
        Me.L10C5.Size = New System.Drawing.Size(49, 50)
        Me.L10C5.TabIndex = 229
        Me.L10C5.TabStop = False
        '
        'L10C4
        '
        Me.L10C4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L10C4.Location = New System.Drawing.Point(156, 459)
        Me.L10C4.Margin = New System.Windows.Forms.Padding(0)
        Me.L10C4.Name = "L10C4"
        Me.L10C4.Size = New System.Drawing.Size(49, 50)
        Me.L10C4.TabIndex = 228
        Me.L10C4.TabStop = False
        '
        'L10C3
        '
        Me.L10C3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L10C3.Location = New System.Drawing.Point(107, 459)
        Me.L10C3.Margin = New System.Windows.Forms.Padding(0)
        Me.L10C3.Name = "L10C3"
        Me.L10C3.Size = New System.Drawing.Size(49, 50)
        Me.L10C3.TabIndex = 227
        Me.L10C3.TabStop = False
        '
        'L10C2
        '
        Me.L10C2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L10C2.Location = New System.Drawing.Point(58, 459)
        Me.L10C2.Margin = New System.Windows.Forms.Padding(0)
        Me.L10C2.Name = "L10C2"
        Me.L10C2.Size = New System.Drawing.Size(49, 50)
        Me.L10C2.TabIndex = 226
        Me.L10C2.TabStop = False
        '
        'L10C1
        '
        Me.L10C1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L10C1.Location = New System.Drawing.Point(9, 459)
        Me.L10C1.Margin = New System.Windows.Forms.Padding(0)
        Me.L10C1.Name = "L10C1"
        Me.L10C1.Size = New System.Drawing.Size(49, 50)
        Me.L10C1.TabIndex = 225
        Me.L10C1.TabStop = False
        '
        'LabelNbtuilerestante
        '
        Me.LabelNbtuilerestante.AutoSize = True
        Me.LabelNbtuilerestante.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelNbtuilerestante.Location = New System.Drawing.Point(104, 754)
        Me.LabelNbtuilerestante.Name = "LabelNbtuilerestante"
        Me.LabelNbtuilerestante.Size = New System.Drawing.Size(16, 18)
        Me.LabelNbtuilerestante.TabIndex = 245
        Me.LabelNbtuilerestante.Text = "0"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(6, 717)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(211, 18)
        Me.Label1.TabIndex = 246
        Me.Label1.Text = "Nombre de tuiles restantes"
        '
        'Case1imagejoueur
        '
        Me.Case1imagejoueur.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Case1imagejoueur.Location = New System.Drawing.Point(303, 717)
        Me.Case1imagejoueur.Margin = New System.Windows.Forms.Padding(0)
        Me.Case1imagejoueur.Name = "Case1imagejoueur"
        Me.Case1imagejoueur.Size = New System.Drawing.Size(49, 50)
        Me.Case1imagejoueur.TabIndex = 247
        Me.Case1imagejoueur.TabStop = False
        '
        'Case6imagejoueur
        '
        Me.Case6imagejoueur.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Case6imagejoueur.Location = New System.Drawing.Point(548, 717)
        Me.Case6imagejoueur.Margin = New System.Windows.Forms.Padding(0)
        Me.Case6imagejoueur.Name = "Case6imagejoueur"
        Me.Case6imagejoueur.Size = New System.Drawing.Size(49, 50)
        Me.Case6imagejoueur.TabIndex = 248
        Me.Case6imagejoueur.TabStop = False
        '
        'Case5imagejoueur
        '
        Me.Case5imagejoueur.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Case5imagejoueur.Location = New System.Drawing.Point(499, 717)
        Me.Case5imagejoueur.Margin = New System.Windows.Forms.Padding(0)
        Me.Case5imagejoueur.Name = "Case5imagejoueur"
        Me.Case5imagejoueur.Size = New System.Drawing.Size(49, 50)
        Me.Case5imagejoueur.TabIndex = 249
        Me.Case5imagejoueur.TabStop = False
        '
        'Case4imagejoueur
        '
        Me.Case4imagejoueur.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Case4imagejoueur.Location = New System.Drawing.Point(450, 717)
        Me.Case4imagejoueur.Margin = New System.Windows.Forms.Padding(0)
        Me.Case4imagejoueur.Name = "Case4imagejoueur"
        Me.Case4imagejoueur.Size = New System.Drawing.Size(49, 50)
        Me.Case4imagejoueur.TabIndex = 250
        Me.Case4imagejoueur.TabStop = False
        '
        'Case3imagejoueur
        '
        Me.Case3imagejoueur.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Case3imagejoueur.Location = New System.Drawing.Point(401, 717)
        Me.Case3imagejoueur.Margin = New System.Windows.Forms.Padding(0)
        Me.Case3imagejoueur.Name = "Case3imagejoueur"
        Me.Case3imagejoueur.Size = New System.Drawing.Size(49, 50)
        Me.Case3imagejoueur.TabIndex = 251
        Me.Case3imagejoueur.TabStop = False
        '
        'Case2imagejoueur
        '
        Me.Case2imagejoueur.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Case2imagejoueur.Location = New System.Drawing.Point(352, 717)
        Me.Case2imagejoueur.Margin = New System.Windows.Forms.Padding(0)
        Me.Case2imagejoueur.Name = "Case2imagejoueur"
        Me.Case2imagejoueur.Size = New System.Drawing.Size(49, 50)
        Me.Case2imagejoueur.TabIndex = 252
        Me.Case2imagejoueur.TabStop = False
        '
        'ButtonAnnuler
        '
        Me.ButtonAnnuler.Location = New System.Drawing.Point(603, 720)
        Me.ButtonAnnuler.Name = "ButtonAnnuler"
        Me.ButtonAnnuler.Size = New System.Drawing.Size(75, 23)
        Me.ButtonAnnuler.TabIndex = 253
        Me.ButtonAnnuler.Text = "Annuler"
        Me.ButtonAnnuler.UseVisualStyleBackColor = True
        '
        'ButtonEchanger
        '
        Me.ButtonEchanger.Location = New System.Drawing.Point(603, 744)
        Me.ButtonEchanger.Name = "ButtonEchanger"
        Me.ButtonEchanger.Size = New System.Drawing.Size(75, 23)
        Me.ButtonEchanger.TabIndex = 254
        Me.ButtonEchanger.Text = "Echanger"
        Me.ButtonEchanger.UseVisualStyleBackColor = True
        '
        'ButtonValider
        '
        Me.ButtonValider.Location = New System.Drawing.Point(684, 732)
        Me.ButtonValider.Name = "ButtonValider"
        Me.ButtonValider.Size = New System.Drawing.Size(75, 23)
        Me.ButtonValider.TabIndex = 255
        Me.ButtonValider.Text = "Valider"
        Me.ButtonValider.UseVisualStyleBackColor = True
        '
        'LabelNomjoueurencours
        '
        Me.LabelNomjoueurencours.AutoSize = True
        Me.LabelNomjoueurencours.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelNomjoueurencours.Location = New System.Drawing.Point(814, 717)
        Me.LabelNomjoueurencours.Name = "LabelNomjoueurencours"
        Me.LabelNomjoueurencours.Size = New System.Drawing.Size(190, 18)
        Me.LabelNomjoueurencours.TabIndex = 256
        Me.LabelNomjoueurencours.Text = "Nom du joueur en cours"
        '
        'Labelpointjoueurencours
        '
        Me.Labelpointjoueurencours.AutoSize = True
        Me.Labelpointjoueurencours.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Labelpointjoueurencours.Location = New System.Drawing.Point(870, 744)
        Me.Labelpointjoueurencours.Name = "Labelpointjoueurencours"
        Me.Labelpointjoueurencours.Size = New System.Drawing.Size(80, 18)
        Me.Labelpointjoueurencours.TabIndex = 257
        Me.Labelpointjoueurencours.Text = "0000points"
        '
        'PictureBox27
        '
        Me.PictureBox27.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox27.Image = Global.interface_projet.My.Resources.Resources.CarreBleu
        Me.PictureBox27.Location = New System.Drawing.Point(107, 59)
        Me.PictureBox27.Margin = New System.Windows.Forms.Padding(0)
        Me.PictureBox27.Name = "PictureBox27"
        Me.PictureBox27.Size = New System.Drawing.Size(49, 50)
        Me.PictureBox27.TabIndex = 0
        Me.PictureBox27.TabStop = False
        '
        'L10C20
        '
        Me.L10C20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L10C20.Location = New System.Drawing.Point(940, 459)
        Me.L10C20.Margin = New System.Windows.Forms.Padding(0)
        Me.L10C20.Name = "L10C20"
        Me.L10C20.Size = New System.Drawing.Size(49, 50)
        Me.L10C20.TabIndex = 244
        Me.L10C20.TabStop = False
        '
        'L11C20
        '
        Me.L11C20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L11C20.Location = New System.Drawing.Point(940, 509)
        Me.L11C20.Margin = New System.Windows.Forms.Padding(0)
        Me.L11C20.Name = "L11C20"
        Me.L11C20.Size = New System.Drawing.Size(49, 50)
        Me.L11C20.TabIndex = 278
        Me.L11C20.TabStop = False
        '
        'PictureBox30
        '
        Me.PictureBox30.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox30.Location = New System.Drawing.Point(940, 509)
        Me.PictureBox30.Margin = New System.Windows.Forms.Padding(0)
        Me.PictureBox30.Name = "PictureBox30"
        Me.PictureBox30.Size = New System.Drawing.Size(49, 50)
        Me.PictureBox30.TabIndex = 277
        Me.PictureBox30.TabStop = False
        '
        'L11C19
        '
        Me.L11C19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L11C19.Location = New System.Drawing.Point(891, 509)
        Me.L11C19.Margin = New System.Windows.Forms.Padding(0)
        Me.L11C19.Name = "L11C19"
        Me.L11C19.Size = New System.Drawing.Size(49, 50)
        Me.L11C19.TabIndex = 276
        Me.L11C19.TabStop = False
        '
        'L11C18
        '
        Me.L11C18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L11C18.Location = New System.Drawing.Point(842, 509)
        Me.L11C18.Margin = New System.Windows.Forms.Padding(0)
        Me.L11C18.Name = "L11C18"
        Me.L11C18.Size = New System.Drawing.Size(49, 50)
        Me.L11C18.TabIndex = 275
        Me.L11C18.TabStop = False
        '
        'L11C17
        '
        Me.L11C17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L11C17.Location = New System.Drawing.Point(793, 509)
        Me.L11C17.Margin = New System.Windows.Forms.Padding(0)
        Me.L11C17.Name = "L11C17"
        Me.L11C17.Size = New System.Drawing.Size(49, 50)
        Me.L11C17.TabIndex = 274
        Me.L11C17.TabStop = False
        '
        'L11C16
        '
        Me.L11C16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L11C16.Location = New System.Drawing.Point(744, 509)
        Me.L11C16.Margin = New System.Windows.Forms.Padding(0)
        Me.L11C16.Name = "L11C16"
        Me.L11C16.Size = New System.Drawing.Size(49, 50)
        Me.L11C16.TabIndex = 273
        Me.L11C16.TabStop = False
        '
        'L11C15
        '
        Me.L11C15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L11C15.Location = New System.Drawing.Point(695, 509)
        Me.L11C15.Margin = New System.Windows.Forms.Padding(0)
        Me.L11C15.Name = "L11C15"
        Me.L11C15.Size = New System.Drawing.Size(49, 50)
        Me.L11C15.TabIndex = 272
        Me.L11C15.TabStop = False
        '
        'L11C14
        '
        Me.L11C14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L11C14.Location = New System.Drawing.Point(646, 509)
        Me.L11C14.Margin = New System.Windows.Forms.Padding(0)
        Me.L11C14.Name = "L11C14"
        Me.L11C14.Size = New System.Drawing.Size(49, 50)
        Me.L11C14.TabIndex = 271
        Me.L11C14.TabStop = False
        '
        'L11C13
        '
        Me.L11C13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L11C13.Location = New System.Drawing.Point(597, 509)
        Me.L11C13.Margin = New System.Windows.Forms.Padding(0)
        Me.L11C13.Name = "L11C13"
        Me.L11C13.Size = New System.Drawing.Size(49, 50)
        Me.L11C13.TabIndex = 270
        Me.L11C13.TabStop = False
        '
        'L11C12
        '
        Me.L11C12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L11C12.Location = New System.Drawing.Point(548, 509)
        Me.L11C12.Margin = New System.Windows.Forms.Padding(0)
        Me.L11C12.Name = "L11C12"
        Me.L11C12.Size = New System.Drawing.Size(49, 50)
        Me.L11C12.TabIndex = 269
        Me.L11C12.TabStop = False
        '
        'L11C11
        '
        Me.L11C11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L11C11.Location = New System.Drawing.Point(499, 509)
        Me.L11C11.Margin = New System.Windows.Forms.Padding(0)
        Me.L11C11.Name = "L11C11"
        Me.L11C11.Size = New System.Drawing.Size(49, 50)
        Me.L11C11.TabIndex = 268
        Me.L11C11.TabStop = False
        '
        'L11C10
        '
        Me.L11C10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L11C10.Location = New System.Drawing.Point(450, 509)
        Me.L11C10.Margin = New System.Windows.Forms.Padding(0)
        Me.L11C10.Name = "L11C10"
        Me.L11C10.Size = New System.Drawing.Size(49, 50)
        Me.L11C10.TabIndex = 267
        Me.L11C10.TabStop = False
        '
        'L11C9
        '
        Me.L11C9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L11C9.Location = New System.Drawing.Point(401, 509)
        Me.L11C9.Margin = New System.Windows.Forms.Padding(0)
        Me.L11C9.Name = "L11C9"
        Me.L11C9.Size = New System.Drawing.Size(49, 50)
        Me.L11C9.TabIndex = 266
        Me.L11C9.TabStop = False
        '
        'L11C8
        '
        Me.L11C8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L11C8.Location = New System.Drawing.Point(352, 509)
        Me.L11C8.Margin = New System.Windows.Forms.Padding(0)
        Me.L11C8.Name = "L11C8"
        Me.L11C8.Size = New System.Drawing.Size(49, 50)
        Me.L11C8.TabIndex = 265
        Me.L11C8.TabStop = False
        '
        'L11C7
        '
        Me.L11C7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L11C7.Location = New System.Drawing.Point(303, 509)
        Me.L11C7.Margin = New System.Windows.Forms.Padding(0)
        Me.L11C7.Name = "L11C7"
        Me.L11C7.Size = New System.Drawing.Size(49, 50)
        Me.L11C7.TabIndex = 264
        Me.L11C7.TabStop = False
        '
        'L11C6
        '
        Me.L11C6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L11C6.Location = New System.Drawing.Point(254, 509)
        Me.L11C6.Margin = New System.Windows.Forms.Padding(0)
        Me.L11C6.Name = "L11C6"
        Me.L11C6.Size = New System.Drawing.Size(49, 50)
        Me.L11C6.TabIndex = 263
        Me.L11C6.TabStop = False
        '
        'L11C5
        '
        Me.L11C5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L11C5.Location = New System.Drawing.Point(205, 509)
        Me.L11C5.Margin = New System.Windows.Forms.Padding(0)
        Me.L11C5.Name = "L11C5"
        Me.L11C5.Size = New System.Drawing.Size(49, 50)
        Me.L11C5.TabIndex = 262
        Me.L11C5.TabStop = False
        '
        'L11C4
        '
        Me.L11C4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L11C4.Location = New System.Drawing.Point(156, 509)
        Me.L11C4.Margin = New System.Windows.Forms.Padding(0)
        Me.L11C4.Name = "L11C4"
        Me.L11C4.Size = New System.Drawing.Size(49, 50)
        Me.L11C4.TabIndex = 261
        Me.L11C4.TabStop = False
        '
        'L11C3
        '
        Me.L11C3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L11C3.Location = New System.Drawing.Point(107, 509)
        Me.L11C3.Margin = New System.Windows.Forms.Padding(0)
        Me.L11C3.Name = "L11C3"
        Me.L11C3.Size = New System.Drawing.Size(49, 50)
        Me.L11C3.TabIndex = 260
        Me.L11C3.TabStop = False
        '
        'L11C2
        '
        Me.L11C2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L11C2.Location = New System.Drawing.Point(58, 509)
        Me.L11C2.Margin = New System.Windows.Forms.Padding(0)
        Me.L11C2.Name = "L11C2"
        Me.L11C2.Size = New System.Drawing.Size(49, 50)
        Me.L11C2.TabIndex = 259
        Me.L11C2.TabStop = False
        '
        'L11C1
        '
        Me.L11C1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L11C1.Location = New System.Drawing.Point(9, 509)
        Me.L11C1.Margin = New System.Windows.Forms.Padding(0)
        Me.L11C1.Name = "L11C1"
        Me.L11C1.Size = New System.Drawing.Size(49, 50)
        Me.L11C1.TabIndex = 258
        Me.L11C1.TabStop = False
        '
        'L12C20
        '
        Me.L12C20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L12C20.Location = New System.Drawing.Point(940, 559)
        Me.L12C20.Margin = New System.Windows.Forms.Padding(0)
        Me.L12C20.Name = "L12C20"
        Me.L12C20.Size = New System.Drawing.Size(49, 50)
        Me.L12C20.TabIndex = 299
        Me.L12C20.TabStop = False
        '
        'PictureBox51
        '
        Me.PictureBox51.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox51.Location = New System.Drawing.Point(940, 559)
        Me.PictureBox51.Margin = New System.Windows.Forms.Padding(0)
        Me.PictureBox51.Name = "PictureBox51"
        Me.PictureBox51.Size = New System.Drawing.Size(49, 50)
        Me.PictureBox51.TabIndex = 298
        Me.PictureBox51.TabStop = False
        '
        'L12C19
        '
        Me.L12C19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L12C19.Location = New System.Drawing.Point(891, 559)
        Me.L12C19.Margin = New System.Windows.Forms.Padding(0)
        Me.L12C19.Name = "L12C19"
        Me.L12C19.Size = New System.Drawing.Size(49, 50)
        Me.L12C19.TabIndex = 297
        Me.L12C19.TabStop = False
        '
        'L12C18
        '
        Me.L12C18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L12C18.Location = New System.Drawing.Point(842, 559)
        Me.L12C18.Margin = New System.Windows.Forms.Padding(0)
        Me.L12C18.Name = "L12C18"
        Me.L12C18.Size = New System.Drawing.Size(49, 50)
        Me.L12C18.TabIndex = 296
        Me.L12C18.TabStop = False
        '
        'L12C17
        '
        Me.L12C17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L12C17.Location = New System.Drawing.Point(793, 559)
        Me.L12C17.Margin = New System.Windows.Forms.Padding(0)
        Me.L12C17.Name = "L12C17"
        Me.L12C17.Size = New System.Drawing.Size(49, 50)
        Me.L12C17.TabIndex = 295
        Me.L12C17.TabStop = False
        '
        'L12C16
        '
        Me.L12C16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L12C16.Location = New System.Drawing.Point(744, 559)
        Me.L12C16.Margin = New System.Windows.Forms.Padding(0)
        Me.L12C16.Name = "L12C16"
        Me.L12C16.Size = New System.Drawing.Size(49, 50)
        Me.L12C16.TabIndex = 294
        Me.L12C16.TabStop = False
        '
        'L12C15
        '
        Me.L12C15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L12C15.Location = New System.Drawing.Point(695, 559)
        Me.L12C15.Margin = New System.Windows.Forms.Padding(0)
        Me.L12C15.Name = "L12C15"
        Me.L12C15.Size = New System.Drawing.Size(49, 50)
        Me.L12C15.TabIndex = 293
        Me.L12C15.TabStop = False
        '
        'L12C14
        '
        Me.L12C14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L12C14.Location = New System.Drawing.Point(646, 559)
        Me.L12C14.Margin = New System.Windows.Forms.Padding(0)
        Me.L12C14.Name = "L12C14"
        Me.L12C14.Size = New System.Drawing.Size(49, 50)
        Me.L12C14.TabIndex = 292
        Me.L12C14.TabStop = False
        '
        'L12C13
        '
        Me.L12C13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L12C13.Location = New System.Drawing.Point(597, 559)
        Me.L12C13.Margin = New System.Windows.Forms.Padding(0)
        Me.L12C13.Name = "L12C13"
        Me.L12C13.Size = New System.Drawing.Size(49, 50)
        Me.L12C13.TabIndex = 291
        Me.L12C13.TabStop = False
        '
        'L12C12
        '
        Me.L12C12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L12C12.Location = New System.Drawing.Point(548, 559)
        Me.L12C12.Margin = New System.Windows.Forms.Padding(0)
        Me.L12C12.Name = "L12C12"
        Me.L12C12.Size = New System.Drawing.Size(49, 50)
        Me.L12C12.TabIndex = 290
        Me.L12C12.TabStop = False
        '
        'L12C11
        '
        Me.L12C11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L12C11.Location = New System.Drawing.Point(499, 559)
        Me.L12C11.Margin = New System.Windows.Forms.Padding(0)
        Me.L12C11.Name = "L12C11"
        Me.L12C11.Size = New System.Drawing.Size(49, 50)
        Me.L12C11.TabIndex = 289
        Me.L12C11.TabStop = False
        '
        'L12C10
        '
        Me.L12C10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L12C10.Location = New System.Drawing.Point(450, 559)
        Me.L12C10.Margin = New System.Windows.Forms.Padding(0)
        Me.L12C10.Name = "L12C10"
        Me.L12C10.Size = New System.Drawing.Size(49, 50)
        Me.L12C10.TabIndex = 288
        Me.L12C10.TabStop = False
        '
        'L12C9
        '
        Me.L12C9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L12C9.Location = New System.Drawing.Point(401, 559)
        Me.L12C9.Margin = New System.Windows.Forms.Padding(0)
        Me.L12C9.Name = "L12C9"
        Me.L12C9.Size = New System.Drawing.Size(49, 50)
        Me.L12C9.TabIndex = 287
        Me.L12C9.TabStop = False
        '
        'L12C8
        '
        Me.L12C8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L12C8.Location = New System.Drawing.Point(352, 559)
        Me.L12C8.Margin = New System.Windows.Forms.Padding(0)
        Me.L12C8.Name = "L12C8"
        Me.L12C8.Size = New System.Drawing.Size(49, 50)
        Me.L12C8.TabIndex = 286
        Me.L12C8.TabStop = False
        '
        'L12C7
        '
        Me.L12C7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L12C7.Location = New System.Drawing.Point(303, 559)
        Me.L12C7.Margin = New System.Windows.Forms.Padding(0)
        Me.L12C7.Name = "L12C7"
        Me.L12C7.Size = New System.Drawing.Size(49, 50)
        Me.L12C7.TabIndex = 285
        Me.L12C7.TabStop = False
        '
        'L12C6
        '
        Me.L12C6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L12C6.Location = New System.Drawing.Point(254, 559)
        Me.L12C6.Margin = New System.Windows.Forms.Padding(0)
        Me.L12C6.Name = "L12C6"
        Me.L12C6.Size = New System.Drawing.Size(49, 50)
        Me.L12C6.TabIndex = 284
        Me.L12C6.TabStop = False
        '
        'L12C5
        '
        Me.L12C5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L12C5.Location = New System.Drawing.Point(205, 559)
        Me.L12C5.Margin = New System.Windows.Forms.Padding(0)
        Me.L12C5.Name = "L12C5"
        Me.L12C5.Size = New System.Drawing.Size(49, 50)
        Me.L12C5.TabIndex = 283
        Me.L12C5.TabStop = False
        '
        'L12C4
        '
        Me.L12C4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L12C4.Location = New System.Drawing.Point(156, 559)
        Me.L12C4.Margin = New System.Windows.Forms.Padding(0)
        Me.L12C4.Name = "L12C4"
        Me.L12C4.Size = New System.Drawing.Size(49, 50)
        Me.L12C4.TabIndex = 282
        Me.L12C4.TabStop = False
        '
        'L12C3
        '
        Me.L12C3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L12C3.Location = New System.Drawing.Point(107, 559)
        Me.L12C3.Margin = New System.Windows.Forms.Padding(0)
        Me.L12C3.Name = "L12C3"
        Me.L12C3.Size = New System.Drawing.Size(49, 50)
        Me.L12C3.TabIndex = 281
        Me.L12C3.TabStop = False
        '
        'L12C2
        '
        Me.L12C2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L12C2.Location = New System.Drawing.Point(58, 559)
        Me.L12C2.Margin = New System.Windows.Forms.Padding(0)
        Me.L12C2.Name = "L12C2"
        Me.L12C2.Size = New System.Drawing.Size(49, 50)
        Me.L12C2.TabIndex = 280
        Me.L12C2.TabStop = False
        '
        'L12C1
        '
        Me.L12C1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L12C1.Location = New System.Drawing.Point(9, 559)
        Me.L12C1.Margin = New System.Windows.Forms.Padding(0)
        Me.L12C1.Name = "L12C1"
        Me.L12C1.Size = New System.Drawing.Size(49, 50)
        Me.L12C1.TabIndex = 279
        Me.L12C1.TabStop = False
        '
        'L13C20
        '
        Me.L13C20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L13C20.Location = New System.Drawing.Point(940, 609)
        Me.L13C20.Margin = New System.Windows.Forms.Padding(0)
        Me.L13C20.Name = "L13C20"
        Me.L13C20.Size = New System.Drawing.Size(49, 50)
        Me.L13C20.TabIndex = 320
        Me.L13C20.TabStop = False
        '
        'PictureBox72
        '
        Me.PictureBox72.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox72.Location = New System.Drawing.Point(940, 609)
        Me.PictureBox72.Margin = New System.Windows.Forms.Padding(0)
        Me.PictureBox72.Name = "PictureBox72"
        Me.PictureBox72.Size = New System.Drawing.Size(49, 50)
        Me.PictureBox72.TabIndex = 319
        Me.PictureBox72.TabStop = False
        '
        'L13C19
        '
        Me.L13C19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L13C19.Location = New System.Drawing.Point(891, 609)
        Me.L13C19.Margin = New System.Windows.Forms.Padding(0)
        Me.L13C19.Name = "L13C19"
        Me.L13C19.Size = New System.Drawing.Size(49, 50)
        Me.L13C19.TabIndex = 318
        Me.L13C19.TabStop = False
        '
        'L13C18
        '
        Me.L13C18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L13C18.Location = New System.Drawing.Point(842, 609)
        Me.L13C18.Margin = New System.Windows.Forms.Padding(0)
        Me.L13C18.Name = "L13C18"
        Me.L13C18.Size = New System.Drawing.Size(49, 50)
        Me.L13C18.TabIndex = 317
        Me.L13C18.TabStop = False
        '
        'L13C17
        '
        Me.L13C17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L13C17.Location = New System.Drawing.Point(793, 609)
        Me.L13C17.Margin = New System.Windows.Forms.Padding(0)
        Me.L13C17.Name = "L13C17"
        Me.L13C17.Size = New System.Drawing.Size(49, 50)
        Me.L13C17.TabIndex = 316
        Me.L13C17.TabStop = False
        '
        'L13C16
        '
        Me.L13C16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L13C16.Location = New System.Drawing.Point(744, 609)
        Me.L13C16.Margin = New System.Windows.Forms.Padding(0)
        Me.L13C16.Name = "L13C16"
        Me.L13C16.Size = New System.Drawing.Size(49, 50)
        Me.L13C16.TabIndex = 315
        Me.L13C16.TabStop = False
        '
        'L13C15
        '
        Me.L13C15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L13C15.Location = New System.Drawing.Point(695, 609)
        Me.L13C15.Margin = New System.Windows.Forms.Padding(0)
        Me.L13C15.Name = "L13C15"
        Me.L13C15.Size = New System.Drawing.Size(49, 50)
        Me.L13C15.TabIndex = 314
        Me.L13C15.TabStop = False
        '
        'L13C14
        '
        Me.L13C14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L13C14.Location = New System.Drawing.Point(646, 609)
        Me.L13C14.Margin = New System.Windows.Forms.Padding(0)
        Me.L13C14.Name = "L13C14"
        Me.L13C14.Size = New System.Drawing.Size(49, 50)
        Me.L13C14.TabIndex = 313
        Me.L13C14.TabStop = False
        '
        'L13C13
        '
        Me.L13C13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L13C13.Location = New System.Drawing.Point(597, 609)
        Me.L13C13.Margin = New System.Windows.Forms.Padding(0)
        Me.L13C13.Name = "L13C13"
        Me.L13C13.Size = New System.Drawing.Size(49, 50)
        Me.L13C13.TabIndex = 312
        Me.L13C13.TabStop = False
        '
        'L13C12
        '
        Me.L13C12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L13C12.Location = New System.Drawing.Point(548, 609)
        Me.L13C12.Margin = New System.Windows.Forms.Padding(0)
        Me.L13C12.Name = "L13C12"
        Me.L13C12.Size = New System.Drawing.Size(49, 50)
        Me.L13C12.TabIndex = 311
        Me.L13C12.TabStop = False
        '
        'L13C11
        '
        Me.L13C11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L13C11.Location = New System.Drawing.Point(499, 609)
        Me.L13C11.Margin = New System.Windows.Forms.Padding(0)
        Me.L13C11.Name = "L13C11"
        Me.L13C11.Size = New System.Drawing.Size(49, 50)
        Me.L13C11.TabIndex = 310
        Me.L13C11.TabStop = False
        '
        'L13C10
        '
        Me.L13C10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L13C10.Location = New System.Drawing.Point(450, 609)
        Me.L13C10.Margin = New System.Windows.Forms.Padding(0)
        Me.L13C10.Name = "L13C10"
        Me.L13C10.Size = New System.Drawing.Size(49, 50)
        Me.L13C10.TabIndex = 309
        Me.L13C10.TabStop = False
        '
        'L13C9
        '
        Me.L13C9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L13C9.Location = New System.Drawing.Point(401, 609)
        Me.L13C9.Margin = New System.Windows.Forms.Padding(0)
        Me.L13C9.Name = "L13C9"
        Me.L13C9.Size = New System.Drawing.Size(49, 50)
        Me.L13C9.TabIndex = 308
        Me.L13C9.TabStop = False
        '
        'L13C8
        '
        Me.L13C8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L13C8.Location = New System.Drawing.Point(352, 609)
        Me.L13C8.Margin = New System.Windows.Forms.Padding(0)
        Me.L13C8.Name = "L13C8"
        Me.L13C8.Size = New System.Drawing.Size(49, 50)
        Me.L13C8.TabIndex = 307
        Me.L13C8.TabStop = False
        '
        'L13C7
        '
        Me.L13C7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L13C7.Location = New System.Drawing.Point(303, 609)
        Me.L13C7.Margin = New System.Windows.Forms.Padding(0)
        Me.L13C7.Name = "L13C7"
        Me.L13C7.Size = New System.Drawing.Size(49, 50)
        Me.L13C7.TabIndex = 306
        Me.L13C7.TabStop = False
        '
        'L13C6
        '
        Me.L13C6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L13C6.Location = New System.Drawing.Point(254, 609)
        Me.L13C6.Margin = New System.Windows.Forms.Padding(0)
        Me.L13C6.Name = "L13C6"
        Me.L13C6.Size = New System.Drawing.Size(49, 50)
        Me.L13C6.TabIndex = 305
        Me.L13C6.TabStop = False
        '
        'L13C5
        '
        Me.L13C5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L13C5.Location = New System.Drawing.Point(205, 609)
        Me.L13C5.Margin = New System.Windows.Forms.Padding(0)
        Me.L13C5.Name = "L13C5"
        Me.L13C5.Size = New System.Drawing.Size(49, 50)
        Me.L13C5.TabIndex = 304
        Me.L13C5.TabStop = False
        '
        'L13C4
        '
        Me.L13C4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L13C4.Location = New System.Drawing.Point(156, 609)
        Me.L13C4.Margin = New System.Windows.Forms.Padding(0)
        Me.L13C4.Name = "L13C4"
        Me.L13C4.Size = New System.Drawing.Size(49, 50)
        Me.L13C4.TabIndex = 303
        Me.L13C4.TabStop = False
        '
        'L13C3
        '
        Me.L13C3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L13C3.Location = New System.Drawing.Point(107, 609)
        Me.L13C3.Margin = New System.Windows.Forms.Padding(0)
        Me.L13C3.Name = "L13C3"
        Me.L13C3.Size = New System.Drawing.Size(49, 50)
        Me.L13C3.TabIndex = 302
        Me.L13C3.TabStop = False
        '
        'L13C2
        '
        Me.L13C2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L13C2.Location = New System.Drawing.Point(58, 609)
        Me.L13C2.Margin = New System.Windows.Forms.Padding(0)
        Me.L13C2.Name = "L13C2"
        Me.L13C2.Size = New System.Drawing.Size(49, 50)
        Me.L13C2.TabIndex = 301
        Me.L13C2.TabStop = False
        '
        'L13C1
        '
        Me.L13C1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.L13C1.Location = New System.Drawing.Point(9, 609)
        Me.L13C1.Margin = New System.Windows.Forms.Padding(0)
        Me.L13C1.Name = "L13C1"
        Me.L13C1.Size = New System.Drawing.Size(49, 50)
        Me.L13C1.TabIndex = 300
        Me.L13C1.TabStop = False
        '
        'PlateauQwirkle
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1008, 812)
        Me.Controls.Add(Me.L13C20)
        Me.Controls.Add(Me.PictureBox72)
        Me.Controls.Add(Me.L13C19)
        Me.Controls.Add(Me.L13C18)
        Me.Controls.Add(Me.L13C17)
        Me.Controls.Add(Me.L13C16)
        Me.Controls.Add(Me.L13C15)
        Me.Controls.Add(Me.L13C14)
        Me.Controls.Add(Me.L13C13)
        Me.Controls.Add(Me.L13C12)
        Me.Controls.Add(Me.L13C11)
        Me.Controls.Add(Me.L13C10)
        Me.Controls.Add(Me.L13C9)
        Me.Controls.Add(Me.L13C8)
        Me.Controls.Add(Me.L13C7)
        Me.Controls.Add(Me.L13C6)
        Me.Controls.Add(Me.L13C5)
        Me.Controls.Add(Me.L13C4)
        Me.Controls.Add(Me.L13C3)
        Me.Controls.Add(Me.L13C2)
        Me.Controls.Add(Me.L13C1)
        Me.Controls.Add(Me.L12C20)
        Me.Controls.Add(Me.PictureBox51)
        Me.Controls.Add(Me.L12C19)
        Me.Controls.Add(Me.L12C18)
        Me.Controls.Add(Me.L12C17)
        Me.Controls.Add(Me.L12C16)
        Me.Controls.Add(Me.L12C15)
        Me.Controls.Add(Me.L12C14)
        Me.Controls.Add(Me.L12C13)
        Me.Controls.Add(Me.L12C12)
        Me.Controls.Add(Me.L12C11)
        Me.Controls.Add(Me.L12C10)
        Me.Controls.Add(Me.L12C9)
        Me.Controls.Add(Me.L12C8)
        Me.Controls.Add(Me.L12C7)
        Me.Controls.Add(Me.L12C6)
        Me.Controls.Add(Me.L12C5)
        Me.Controls.Add(Me.L12C4)
        Me.Controls.Add(Me.L12C3)
        Me.Controls.Add(Me.L12C2)
        Me.Controls.Add(Me.L12C1)
        Me.Controls.Add(Me.L11C20)
        Me.Controls.Add(Me.PictureBox30)
        Me.Controls.Add(Me.L11C19)
        Me.Controls.Add(Me.L11C18)
        Me.Controls.Add(Me.L11C17)
        Me.Controls.Add(Me.L11C16)
        Me.Controls.Add(Me.L11C15)
        Me.Controls.Add(Me.L11C14)
        Me.Controls.Add(Me.L11C13)
        Me.Controls.Add(Me.L11C12)
        Me.Controls.Add(Me.L11C11)
        Me.Controls.Add(Me.L11C10)
        Me.Controls.Add(Me.L11C9)
        Me.Controls.Add(Me.L11C8)
        Me.Controls.Add(Me.L11C7)
        Me.Controls.Add(Me.L11C6)
        Me.Controls.Add(Me.L11C5)
        Me.Controls.Add(Me.L11C4)
        Me.Controls.Add(Me.L11C3)
        Me.Controls.Add(Me.L11C2)
        Me.Controls.Add(Me.L11C1)
        Me.Controls.Add(Me.Labelpointjoueurencours)
        Me.Controls.Add(Me.LabelNomjoueurencours)
        Me.Controls.Add(Me.ButtonValider)
        Me.Controls.Add(Me.ButtonEchanger)
        Me.Controls.Add(Me.ButtonAnnuler)
        Me.Controls.Add(Me.Case2imagejoueur)
        Me.Controls.Add(Me.Case3imagejoueur)
        Me.Controls.Add(Me.Case4imagejoueur)
        Me.Controls.Add(Me.Case5imagejoueur)
        Me.Controls.Add(Me.Case6imagejoueur)
        Me.Controls.Add(Me.Case1imagejoueur)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.LabelNbtuilerestante)
        Me.Controls.Add(Me.L10C20)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.L10C19)
        Me.Controls.Add(Me.L10C18)
        Me.Controls.Add(Me.L10C17)
        Me.Controls.Add(Me.L10C16)
        Me.Controls.Add(Me.L10C15)
        Me.Controls.Add(Me.L10C14)
        Me.Controls.Add(Me.L10C13)
        Me.Controls.Add(Me.L10C12)
        Me.Controls.Add(Me.L10C11)
        Me.Controls.Add(Me.L10C10)
        Me.Controls.Add(Me.L10C9)
        Me.Controls.Add(Me.L10C8)
        Me.Controls.Add(Me.L10C7)
        Me.Controls.Add(Me.L10C6)
        Me.Controls.Add(Me.L10C5)
        Me.Controls.Add(Me.L10C4)
        Me.Controls.Add(Me.L10C3)
        Me.Controls.Add(Me.L10C2)
        Me.Controls.Add(Me.L10C1)
        Me.Controls.Add(Me.L9C20)
        Me.Controls.Add(Me.L9C19)
        Me.Controls.Add(Me.L9C18)
        Me.Controls.Add(Me.L9C17)
        Me.Controls.Add(Me.L9C16)
        Me.Controls.Add(Me.L9C15)
        Me.Controls.Add(Me.L9C14)
        Me.Controls.Add(Me.L9C13)
        Me.Controls.Add(Me.L9C12)
        Me.Controls.Add(Me.L9C11)
        Me.Controls.Add(Me.L9C10)
        Me.Controls.Add(Me.L9C9)
        Me.Controls.Add(Me.L9C8)
        Me.Controls.Add(Me.L9C7)
        Me.Controls.Add(Me.L9C6)
        Me.Controls.Add(Me.L9C5)
        Me.Controls.Add(Me.L9C4)
        Me.Controls.Add(Me.L9C3)
        Me.Controls.Add(Me.L9C2)
        Me.Controls.Add(Me.L9C1)
        Me.Controls.Add(Me.L8C20)
        Me.Controls.Add(Me.L8C19)
        Me.Controls.Add(Me.L8C18)
        Me.Controls.Add(Me.L8C17)
        Me.Controls.Add(Me.L8C16)
        Me.Controls.Add(Me.L8C15)
        Me.Controls.Add(Me.L8C14)
        Me.Controls.Add(Me.L8C13)
        Me.Controls.Add(Me.L8C12)
        Me.Controls.Add(Me.L8C11)
        Me.Controls.Add(Me.L8C10)
        Me.Controls.Add(Me.L8C9)
        Me.Controls.Add(Me.L8C8)
        Me.Controls.Add(Me.L8C7)
        Me.Controls.Add(Me.L8C6)
        Me.Controls.Add(Me.L8C5)
        Me.Controls.Add(Me.L8C4)
        Me.Controls.Add(Me.L8C3)
        Me.Controls.Add(Me.L8C2)
        Me.Controls.Add(Me.L8C1)
        Me.Controls.Add(Me.L7C20)
        Me.Controls.Add(Me.L7C19)
        Me.Controls.Add(Me.L7C18)
        Me.Controls.Add(Me.L7C17)
        Me.Controls.Add(Me.L7C16)
        Me.Controls.Add(Me.L7C15)
        Me.Controls.Add(Me.L7C14)
        Me.Controls.Add(Me.L7C13)
        Me.Controls.Add(Me.L7C12)
        Me.Controls.Add(Me.L7C11)
        Me.Controls.Add(Me.L7C10)
        Me.Controls.Add(Me.L7C9)
        Me.Controls.Add(Me.L7C8)
        Me.Controls.Add(Me.L7C7)
        Me.Controls.Add(Me.L7C6)
        Me.Controls.Add(Me.L7C5)
        Me.Controls.Add(Me.L7C4)
        Me.Controls.Add(Me.L7C3)
        Me.Controls.Add(Me.L7C2)
        Me.Controls.Add(Me.L7C1)
        Me.Controls.Add(Me.L6C20)
        Me.Controls.Add(Me.L6C19)
        Me.Controls.Add(Me.L6C18)
        Me.Controls.Add(Me.L6C17)
        Me.Controls.Add(Me.L6C16)
        Me.Controls.Add(Me.L6C15)
        Me.Controls.Add(Me.L6C14)
        Me.Controls.Add(Me.L6C13)
        Me.Controls.Add(Me.L6C12)
        Me.Controls.Add(Me.L6C11)
        Me.Controls.Add(Me.L6C10)
        Me.Controls.Add(Me.L6C9)
        Me.Controls.Add(Me.L6C8)
        Me.Controls.Add(Me.L6C7)
        Me.Controls.Add(Me.L6C6)
        Me.Controls.Add(Me.L6C5)
        Me.Controls.Add(Me.L6C4)
        Me.Controls.Add(Me.L6C3)
        Me.Controls.Add(Me.L6C2)
        Me.Controls.Add(Me.L6C1)
        Me.Controls.Add(Me.L5C20)
        Me.Controls.Add(Me.L5C19)
        Me.Controls.Add(Me.L5C18)
        Me.Controls.Add(Me.L5C17)
        Me.Controls.Add(Me.L5C16)
        Me.Controls.Add(Me.L5C15)
        Me.Controls.Add(Me.L5C14)
        Me.Controls.Add(Me.L5C13)
        Me.Controls.Add(Me.L5C12)
        Me.Controls.Add(Me.L5C11)
        Me.Controls.Add(Me.L5C10)
        Me.Controls.Add(Me.L5C9)
        Me.Controls.Add(Me.L5C8)
        Me.Controls.Add(Me.L5C7)
        Me.Controls.Add(Me.L5C6)
        Me.Controls.Add(Me.L5C5)
        Me.Controls.Add(Me.L5C4)
        Me.Controls.Add(Me.L5C3)
        Me.Controls.Add(Me.L5C2)
        Me.Controls.Add(Me.L5C1)
        Me.Controls.Add(Me.L4C20)
        Me.Controls.Add(Me.L4C19)
        Me.Controls.Add(Me.L4C18)
        Me.Controls.Add(Me.L4C17)
        Me.Controls.Add(Me.L4C16)
        Me.Controls.Add(Me.L4C15)
        Me.Controls.Add(Me.L4C14)
        Me.Controls.Add(Me.L4C13)
        Me.Controls.Add(Me.L4C12)
        Me.Controls.Add(Me.L4C11)
        Me.Controls.Add(Me.L4C10)
        Me.Controls.Add(Me.L4C9)
        Me.Controls.Add(Me.L4C8)
        Me.Controls.Add(Me.L4C7)
        Me.Controls.Add(Me.L4C6)
        Me.Controls.Add(Me.L4C5)
        Me.Controls.Add(Me.L4C4)
        Me.Controls.Add(Me.L4C3)
        Me.Controls.Add(Me.L4C2)
        Me.Controls.Add(Me.L4C1)
        Me.Controls.Add(Me.L3C20)
        Me.Controls.Add(Me.L3C19)
        Me.Controls.Add(Me.L3C18)
        Me.Controls.Add(Me.L3C17)
        Me.Controls.Add(Me.L3C16)
        Me.Controls.Add(Me.L3C15)
        Me.Controls.Add(Me.L3C14)
        Me.Controls.Add(Me.L3C13)
        Me.Controls.Add(Me.L3C12)
        Me.Controls.Add(Me.L3C11)
        Me.Controls.Add(Me.L3C10)
        Me.Controls.Add(Me.L3C9)
        Me.Controls.Add(Me.L3C8)
        Me.Controls.Add(Me.L3C7)
        Me.Controls.Add(Me.L3C6)
        Me.Controls.Add(Me.L3C5)
        Me.Controls.Add(Me.L3C4)
        Me.Controls.Add(Me.L3C3)
        Me.Controls.Add(Me.L3C2)
        Me.Controls.Add(Me.L3C1)
        Me.Controls.Add(Me.L2C20)
        Me.Controls.Add(Me.L2C19)
        Me.Controls.Add(Me.L2C18)
        Me.Controls.Add(Me.L2C17)
        Me.Controls.Add(Me.L2C16)
        Me.Controls.Add(Me.L2C15)
        Me.Controls.Add(Me.L2C14)
        Me.Controls.Add(Me.L2C13)
        Me.Controls.Add(Me.L2C12)
        Me.Controls.Add(Me.L2C11)
        Me.Controls.Add(Me.L2C10)
        Me.Controls.Add(Me.L2C9)
        Me.Controls.Add(Me.L2C8)
        Me.Controls.Add(Me.L2C7)
        Me.Controls.Add(Me.L2C6)
        Me.Controls.Add(Me.L2C5)
        Me.Controls.Add(Me.L2C4)
        Me.Controls.Add(Me.L2C3)
        Me.Controls.Add(Me.L2C2)
        Me.Controls.Add(Me.L2C1)
        Me.Controls.Add(Me.L1C20)
        Me.Controls.Add(Me.L1C19)
        Me.Controls.Add(Me.L1C18)
        Me.Controls.Add(Me.L1C17)
        Me.Controls.Add(Me.L1C16)
        Me.Controls.Add(Me.L1C15)
        Me.Controls.Add(Me.L1C14)
        Me.Controls.Add(Me.L1C13)
        Me.Controls.Add(Me.L1C12)
        Me.Controls.Add(Me.L1C11)
        Me.Controls.Add(Me.L1C10)
        Me.Controls.Add(Me.L1C9)
        Me.Controls.Add(Me.L1C8)
        Me.Controls.Add(Me.L1C7)
        Me.Controls.Add(Me.L1C6)
        Me.Controls.Add(Me.L1C5)
        Me.Controls.Add(Me.L1C4)
        Me.Controls.Add(Me.L1C3)
        Me.Controls.Add(Me.L1C2)
        Me.Controls.Add(Me.PictureBox27)
        Me.Controls.Add(Me.L1C1)
        Me.Name = "PlateauQwirkle"
        Me.Text = "PlateauQwirkle"
        CType(Me.L1C1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L1C2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L1C4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L1C3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L1C5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L1C10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L1C9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L1C8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L1C7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L1C6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L1C15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L1C14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L1C13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L1C12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L1C11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L1C20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L1C19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L1C18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L1C17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L1C16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L2C20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L2C19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L2C18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L2C17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L2C16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L2C15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L2C14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L2C13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L2C12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L2C11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L2C10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L2C9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L2C8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L2C7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L2C6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L2C5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L2C4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L2C3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L2C2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L2C1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L3C20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L3C19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L3C18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L3C17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L3C16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L3C15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L3C14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L3C13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L3C12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L3C11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L3C10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L3C9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L3C8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L3C7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L3C6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L3C5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L3C4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L3C3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L3C2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L3C1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L4C20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L4C19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L4C18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L4C17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L4C16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L4C15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L4C14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L4C13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L4C12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L4C11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L4C10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L4C9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L4C8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L4C7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L4C6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L4C5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L4C4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L4C3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L4C2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L4C1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L5C20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L5C19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L5C18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L5C17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L5C16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L5C15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L5C14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L5C13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L5C12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L5C11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L5C10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L5C9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L5C8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L5C7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L5C6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L5C5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L5C4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L5C3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L5C2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L5C1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L6C20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L6C19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L6C18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L6C17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L6C16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L6C15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L6C14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L6C13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L6C12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L6C11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L6C10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L6C9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L6C8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L6C7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L6C6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L6C5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L6C4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L6C3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L6C2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L6C1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L7C20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L7C19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L7C18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L7C17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L7C16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L7C15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L7C14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L7C13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L7C12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L7C11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L7C10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L7C9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L7C8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L7C7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L7C6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L7C5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L7C4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L7C3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L7C2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L7C1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L9C20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L9C19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L9C18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L9C17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L9C16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L9C15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L9C14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L9C13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L9C12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L9C11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L9C10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L9C9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L9C8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L9C7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L9C6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L9C5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L9C4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L9C3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L9C2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L9C1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L8C20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L8C19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L8C18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L8C17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L8C16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L8C15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L8C14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L8C13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L8C12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L8C11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L8C10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L8C9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L8C8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L8C7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L8C6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L8C5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L8C4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L8C3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L8C2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L8C1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L10C19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L10C18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L10C17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L10C16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L10C15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L10C14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L10C13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L10C12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L10C11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L10C10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L10C9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L10C8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L10C7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L10C6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L10C5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L10C4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L10C3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L10C2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L10C1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Case1imagejoueur, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Case6imagejoueur, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Case5imagejoueur, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Case4imagejoueur, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Case3imagejoueur, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Case2imagejoueur, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox27, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L10C20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L11C20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox30, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L11C19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L11C18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L11C17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L11C16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L11C15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L11C14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L11C13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L11C12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L11C11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L11C10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L11C9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L11C8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L11C7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L11C6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L11C5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L11C4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L11C3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L11C2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L11C1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L12C20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox51, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L12C19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L12C18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L12C17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L12C16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L12C15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L12C14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L12C13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L12C12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L12C11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L12C10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L12C9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L12C8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L12C7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L12C6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L12C5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L12C4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L12C3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L12C2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L12C1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L13C20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox72, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L13C19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L13C18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L13C17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L13C16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L13C15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L13C14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L13C13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L13C12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L13C11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L13C10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L13C9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L13C8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L13C7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L13C6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L13C5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L13C4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L13C3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L13C2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.L13C1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents L1C1 As PictureBox
    Friend WithEvents L1C2 As PictureBox
    Friend WithEvents L1C4 As PictureBox
    Friend WithEvents L1C3 As PictureBox
    Friend WithEvents L1C5 As PictureBox
    Friend WithEvents L1C10 As PictureBox
    Friend WithEvents L1C9 As PictureBox
    Friend WithEvents L1C8 As PictureBox
    Friend WithEvents L1C7 As PictureBox
    Friend WithEvents L1C6 As PictureBox
    Friend WithEvents L1C15 As PictureBox
    Friend WithEvents L1C14 As PictureBox
    Friend WithEvents L1C13 As PictureBox
    Friend WithEvents L1C12 As PictureBox
    Friend WithEvents L1C11 As PictureBox
    Friend WithEvents L1C20 As PictureBox
    Friend WithEvents L1C19 As PictureBox
    Friend WithEvents L1C18 As PictureBox
    Friend WithEvents L1C17 As PictureBox
    Friend WithEvents L1C16 As PictureBox
    Friend WithEvents L2C20 As PictureBox
    Friend WithEvents L2C19 As PictureBox
    Friend WithEvents L2C18 As PictureBox
    Friend WithEvents L2C17 As PictureBox
    Friend WithEvents L2C16 As PictureBox
    Friend WithEvents L2C15 As PictureBox
    Friend WithEvents L2C14 As PictureBox
    Friend WithEvents L2C13 As PictureBox
    Friend WithEvents L2C12 As PictureBox
    Friend WithEvents L2C11 As PictureBox
    Friend WithEvents L2C10 As PictureBox
    Friend WithEvents L2C9 As PictureBox
    Friend WithEvents L2C8 As PictureBox
    Friend WithEvents L2C7 As PictureBox
    Friend WithEvents L2C6 As PictureBox
    Friend WithEvents L2C5 As PictureBox
    Friend WithEvents L2C4 As PictureBox
    Friend WithEvents L2C3 As PictureBox
    Friend WithEvents L2C2 As PictureBox
    Friend WithEvents L2C1 As PictureBox
    Friend WithEvents L3C20 As PictureBox
    Friend WithEvents L3C19 As PictureBox
    Friend WithEvents L3C18 As PictureBox
    Friend WithEvents L3C17 As PictureBox
    Friend WithEvents L3C16 As PictureBox
    Friend WithEvents L3C15 As PictureBox
    Friend WithEvents L3C14 As PictureBox
    Friend WithEvents L3C13 As PictureBox
    Friend WithEvents L3C12 As PictureBox
    Friend WithEvents L3C11 As PictureBox
    Friend WithEvents L3C10 As PictureBox
    Friend WithEvents L3C9 As PictureBox
    Friend WithEvents L3C8 As PictureBox
    Friend WithEvents L3C7 As PictureBox
    Friend WithEvents L3C6 As PictureBox
    Friend WithEvents L3C5 As PictureBox
    Friend WithEvents L3C4 As PictureBox
    Friend WithEvents L3C3 As PictureBox
    Friend WithEvents L3C2 As PictureBox
    Friend WithEvents L3C1 As PictureBox
    Friend WithEvents L4C20 As PictureBox
    Friend WithEvents L4C19 As PictureBox
    Friend WithEvents L4C18 As PictureBox
    Friend WithEvents L4C17 As PictureBox
    Friend WithEvents L4C16 As PictureBox
    Friend WithEvents L4C15 As PictureBox
    Friend WithEvents L4C14 As PictureBox
    Friend WithEvents L4C13 As PictureBox
    Friend WithEvents L4C12 As PictureBox
    Friend WithEvents L4C11 As PictureBox
    Friend WithEvents L4C10 As PictureBox
    Friend WithEvents L4C9 As PictureBox
    Friend WithEvents L4C8 As PictureBox
    Friend WithEvents L4C7 As PictureBox
    Friend WithEvents L4C6 As PictureBox
    Friend WithEvents L4C5 As PictureBox
    Friend WithEvents L4C4 As PictureBox
    Friend WithEvents L4C3 As PictureBox
    Friend WithEvents L4C2 As PictureBox
    Friend WithEvents L4C1 As PictureBox
    Friend WithEvents L5C20 As PictureBox
    Friend WithEvents L5C19 As PictureBox
    Friend WithEvents L5C18 As PictureBox
    Friend WithEvents L5C17 As PictureBox
    Friend WithEvents L5C16 As PictureBox
    Friend WithEvents L5C15 As PictureBox
    Friend WithEvents L5C14 As PictureBox
    Friend WithEvents L5C13 As PictureBox
    Friend WithEvents L5C12 As PictureBox
    Friend WithEvents L5C11 As PictureBox
    Friend WithEvents L5C10 As PictureBox
    Friend WithEvents L5C9 As PictureBox
    Friend WithEvents L5C8 As PictureBox
    Friend WithEvents L5C7 As PictureBox
    Friend WithEvents L5C6 As PictureBox
    Friend WithEvents L5C5 As PictureBox
    Friend WithEvents L5C4 As PictureBox
    Friend WithEvents L5C3 As PictureBox
    Friend WithEvents L5C2 As PictureBox
    Friend WithEvents L5C1 As PictureBox
    Friend WithEvents L6C20 As PictureBox
    Friend WithEvents L6C19 As PictureBox
    Friend WithEvents L6C18 As PictureBox
    Friend WithEvents L6C17 As PictureBox
    Friend WithEvents L6C16 As PictureBox
    Friend WithEvents L6C15 As PictureBox
    Friend WithEvents L6C14 As PictureBox
    Friend WithEvents L6C13 As PictureBox
    Friend WithEvents L6C12 As PictureBox
    Friend WithEvents L6C11 As PictureBox
    Friend WithEvents L6C10 As PictureBox
    Friend WithEvents L6C9 As PictureBox
    Friend WithEvents L6C8 As PictureBox
    Friend WithEvents L6C7 As PictureBox
    Friend WithEvents L6C6 As PictureBox
    Friend WithEvents L6C5 As PictureBox
    Friend WithEvents L6C4 As PictureBox
    Friend WithEvents L6C3 As PictureBox
    Friend WithEvents L6C2 As PictureBox
    Friend WithEvents L6C1 As PictureBox
    Friend WithEvents L7C20 As PictureBox
    Friend WithEvents L7C19 As PictureBox
    Friend WithEvents L7C18 As PictureBox
    Friend WithEvents L7C17 As PictureBox
    Friend WithEvents L7C16 As PictureBox
    Friend WithEvents L7C15 As PictureBox
    Friend WithEvents L7C14 As PictureBox
    Friend WithEvents L7C13 As PictureBox
    Friend WithEvents L7C12 As PictureBox
    Friend WithEvents L7C11 As PictureBox
    Friend WithEvents L7C10 As PictureBox
    Friend WithEvents L7C9 As PictureBox
    Friend WithEvents L7C8 As PictureBox
    Friend WithEvents L7C7 As PictureBox
    Friend WithEvents L7C6 As PictureBox
    Friend WithEvents L7C5 As PictureBox
    Friend WithEvents L7C4 As PictureBox
    Friend WithEvents L7C3 As PictureBox
    Friend WithEvents L7C2 As PictureBox
    Friend WithEvents L7C1 As PictureBox
    Friend WithEvents L9C20 As PictureBox
    Friend WithEvents L9C19 As PictureBox
    Friend WithEvents L9C18 As PictureBox
    Friend WithEvents L9C17 As PictureBox
    Friend WithEvents L9C16 As PictureBox
    Friend WithEvents L9C15 As PictureBox
    Friend WithEvents L9C14 As PictureBox
    Friend WithEvents L9C13 As PictureBox
    Friend WithEvents L9C12 As PictureBox
    Friend WithEvents L9C11 As PictureBox
    Friend WithEvents L9C10 As PictureBox
    Friend WithEvents L9C9 As PictureBox
    Friend WithEvents L9C8 As PictureBox
    Friend WithEvents L9C7 As PictureBox
    Friend WithEvents L9C6 As PictureBox
    Friend WithEvents L9C5 As PictureBox
    Friend WithEvents L9C4 As PictureBox
    Friend WithEvents L9C3 As PictureBox
    Friend WithEvents L9C2 As PictureBox
    Friend WithEvents L9C1 As PictureBox
    Friend WithEvents L8C20 As PictureBox
    Friend WithEvents L8C19 As PictureBox
    Friend WithEvents L8C18 As PictureBox
    Friend WithEvents L8C17 As PictureBox
    Friend WithEvents L8C16 As PictureBox
    Friend WithEvents L8C15 As PictureBox
    Friend WithEvents L8C14 As PictureBox
    Friend WithEvents L8C13 As PictureBox
    Friend WithEvents L8C12 As PictureBox
    Friend WithEvents L8C11 As PictureBox
    Friend WithEvents L8C10 As PictureBox
    Friend WithEvents L8C9 As PictureBox
    Friend WithEvents L8C8 As PictureBox
    Friend WithEvents L8C7 As PictureBox
    Friend WithEvents L8C6 As PictureBox
    Friend WithEvents L8C5 As PictureBox
    Friend WithEvents L8C4 As PictureBox
    Friend WithEvents L8C3 As PictureBox
    Friend WithEvents L8C2 As PictureBox
    Friend WithEvents L8C1 As PictureBox
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents L10C19 As PictureBox
    Friend WithEvents L10C18 As PictureBox
    Friend WithEvents L10C17 As PictureBox
    Friend WithEvents L10C16 As PictureBox
    Friend WithEvents L10C15 As PictureBox
    Friend WithEvents L10C14 As PictureBox
    Friend WithEvents L10C13 As PictureBox
    Friend WithEvents L10C12 As PictureBox
    Friend WithEvents L10C11 As PictureBox
    Friend WithEvents L10C10 As PictureBox
    Friend WithEvents L10C9 As PictureBox
    Friend WithEvents L10C8 As PictureBox
    Friend WithEvents L10C7 As PictureBox
    Friend WithEvents L10C6 As PictureBox
    Friend WithEvents L10C5 As PictureBox
    Friend WithEvents L10C4 As PictureBox
    Friend WithEvents L10C3 As PictureBox
    Friend WithEvents L10C2 As PictureBox
    Friend WithEvents L10C1 As PictureBox
    Friend WithEvents LabelNbtuilerestante As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents Case1imagejoueur As PictureBox
    Friend WithEvents Case6imagejoueur As PictureBox
    Friend WithEvents Case5imagejoueur As PictureBox
    Friend WithEvents Case4imagejoueur As PictureBox
    Friend WithEvents Case3imagejoueur As PictureBox
    Friend WithEvents Case2imagejoueur As PictureBox
    Friend WithEvents ButtonAnnuler As Button
    Friend WithEvents ButtonEchanger As Button
    Friend WithEvents ButtonValider As Button
    Friend WithEvents LabelNomjoueurencours As Label
    Friend WithEvents Labelpointjoueurencours As Label
    Friend WithEvents PictureBox27 As PictureBox
    Friend WithEvents L10C20 As PictureBox
    Friend WithEvents L11C20 As PictureBox
    Friend WithEvents PictureBox30 As PictureBox
    Friend WithEvents L11C19 As PictureBox
    Friend WithEvents L11C18 As PictureBox
    Friend WithEvents L11C17 As PictureBox
    Friend WithEvents L11C16 As PictureBox
    Friend WithEvents L11C15 As PictureBox
    Friend WithEvents L11C14 As PictureBox
    Friend WithEvents L11C13 As PictureBox
    Friend WithEvents L11C12 As PictureBox
    Friend WithEvents L11C11 As PictureBox
    Friend WithEvents L11C10 As PictureBox
    Friend WithEvents L11C9 As PictureBox
    Friend WithEvents L11C8 As PictureBox
    Friend WithEvents L11C7 As PictureBox
    Friend WithEvents L11C6 As PictureBox
    Friend WithEvents L11C5 As PictureBox
    Friend WithEvents L11C4 As PictureBox
    Friend WithEvents L11C3 As PictureBox
    Friend WithEvents L11C2 As PictureBox
    Friend WithEvents L11C1 As PictureBox
    Friend WithEvents L12C20 As PictureBox
    Friend WithEvents PictureBox51 As PictureBox
    Friend WithEvents L12C19 As PictureBox
    Friend WithEvents L12C18 As PictureBox
    Friend WithEvents L12C17 As PictureBox
    Friend WithEvents L12C16 As PictureBox
    Friend WithEvents L12C15 As PictureBox
    Friend WithEvents L12C14 As PictureBox
    Friend WithEvents L12C13 As PictureBox
    Friend WithEvents L12C12 As PictureBox
    Friend WithEvents L12C11 As PictureBox
    Friend WithEvents L12C10 As PictureBox
    Friend WithEvents L12C9 As PictureBox
    Friend WithEvents L12C8 As PictureBox
    Friend WithEvents L12C7 As PictureBox
    Friend WithEvents L12C6 As PictureBox
    Friend WithEvents L12C5 As PictureBox
    Friend WithEvents L12C4 As PictureBox
    Friend WithEvents L12C3 As PictureBox
    Friend WithEvents L12C2 As PictureBox
    Friend WithEvents L12C1 As PictureBox
    Friend WithEvents L13C20 As PictureBox
    Friend WithEvents PictureBox72 As PictureBox
    Friend WithEvents L13C19 As PictureBox
    Friend WithEvents L13C18 As PictureBox
    Friend WithEvents L13C17 As PictureBox
    Friend WithEvents L13C16 As PictureBox
    Friend WithEvents L13C15 As PictureBox
    Friend WithEvents L13C14 As PictureBox
    Friend WithEvents L13C13 As PictureBox
    Friend WithEvents L13C12 As PictureBox
    Friend WithEvents L13C11 As PictureBox
    Friend WithEvents L13C10 As PictureBox
    Friend WithEvents L13C9 As PictureBox
    Friend WithEvents L13C8 As PictureBox
    Friend WithEvents L13C7 As PictureBox
    Friend WithEvents L13C6 As PictureBox
    Friend WithEvents L13C5 As PictureBox
    Friend WithEvents L13C4 As PictureBox
    Friend WithEvents L13C3 As PictureBox
    Friend WithEvents L13C2 As PictureBox
    Friend WithEvents L13C1 As PictureBox
End Class
